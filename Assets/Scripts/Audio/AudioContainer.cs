﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioContainer : MonoBehaviour
{
    public AudioManager mainManager;
    private void Start()
    {
        mainManager = AudioManager.audioSourceInstance;
    }

    public void ChangeGeneralVolume(float value)
    {
        mainManager.mainMixer.SetFloat("GeneralVolume", value);
    }
    public void ChangeMusiclVolume(float value)
    {
        mainManager.mainMixer.SetFloat("MusicVolume", value);
    }
    public void ChangeSfxVolume(float value)
    {
        mainManager.mainMixer.SetFloat("SFXVolume", value);
    }
}
