﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
public class AudioManager : MonoBehaviour
{
    public static AudioManager audioSourceInstance;
    public AudioMixer mainMixer;


    public AudioSource audioSource;
    public AudioClip clickSound;
    private void Awake()
    {
        if (audioSourceInstance != null && audioSourceInstance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            audioSourceInstance = this;
            DontDestroyOnLoad(gameObject);
        }

    }

    public void ChangeGeneralVolume(float value)
    {
        mainMixer.SetFloat("GeneralVolume", value);
    }
    public void ChangeMusiclVolume(float value)
    {
        mainMixer.SetFloat("MusicVolume", value);
    }
    public void ChangeSfxVolume(float value)
    {
        mainMixer.SetFloat("SFXVolume", value);
    }

    public void PlayGeneralClickSound()
    {
        audioSource.PlayOneShot(clickSound);
    }

}
