﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAudioController : MonoBehaviour
{
    public AudioSource audioS;
    public AudioClip stepAudio;

    public void PlayStep()
    {
        audioS.pitch = Random.Range(0.6f , 1.4f);
        audioS.PlayOneShot(stepAudio);
    }
}
