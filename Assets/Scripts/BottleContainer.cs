﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BottleContainer : MonoBehaviour
{
    public int liquidAmount, liquidMax=100;
    public Image liquidImg;
    public void Drink()
    {
        liquidAmount -= 20;
        CharacterAttributeManager.characterAttributeManagerInstance.SumarSed(20);
        FillImageAmount();
    }

    public void Fill()
    {
        liquidAmount = liquidMax;
        FillImageAmount();
    }

    public void FillImageAmount()
    {
        liquidImg.fillAmount = (float)liquidAmount / liquidMax;
    }
}
