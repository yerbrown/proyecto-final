﻿using System.Diagnostics;
using UnityEngine;
using UnityEngine.EventSystems;

public class CameraControll : MonoBehaviour
{
    public Camera cam;
    public Transform target, player;
    public Vector3 targetZoom, zoomFactor = 3 * Vector3.one;
    public float smothSpeed = 0.125f;
    public Vector3 offset, offsetMin, offsetMax;
    public float zoomSpeed = 10;
    public bool minimap = false;
    public GameObject particlesPointer;
    public bool zoomActivated;
    public float startAngle;

    private void Start()
    {
        cam = gameObject.GetComponent<Camera>();
        startAngle = transform.eulerAngles.x;
        transform.position = target.position + offset;
    }
    private void LateUpdate()
    {
        if (minimap)
        {
            transform.position = new Vector3(target.transform.position.x, transform.position.y, target.transform.position.z);

        }
        else
        {
            if (zoomActivated)
            {
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(startAngle, 0, 0), 3 * Time.deltaTime);
                Vector3 desiredPosition = target.position + offset;
                Vector3 smothPosition = Vector3.Lerp(transform.position, desiredPosition, smothSpeed);

                Zoom();
                transform.position = smothPosition;
                //transform.LookAt(target.position + Vector3.up);
                Pointer();
            }
            else
            {
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0,0,0), 3*Time.deltaTime);
                Vector3 desiredPosition = target.position;
                Vector3 smothPosition = Vector3.Lerp(transform.position, desiredPosition, smothSpeed);
                transform.position = smothPosition;

            }
        }

    }
    void Zoom()
    {
        float scrollData;
        scrollData = Input.GetAxis("Mouse ScrollWheel");
        targetZoom -= scrollData * zoomFactor;
        if (targetZoom.y <= offsetMin.y)
        {
            targetZoom = offsetMin;

        }
        if (targetZoom.y > offsetMax.y)
        {
            targetZoom = offsetMax;

        }
        offset = Vector3.Lerp(offset, targetZoom, Time.deltaTime * zoomSpeed);
    }
    void Pointer()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                if (!EventSystem.current.IsPointerOverGameObject())
                {
                    if (hit.collider.CompareTag("Floor"))
                    {
                        particlesPointer.GetComponent<ParticleSystem>().Clear();
                        particlesPointer.GetComponent<ParticleSystem>().Play();
                        particlesPointer.transform.position = hit.point + Vector3.up * 0.05f;
                    }
                    else
                    {
                        particlesPointer.transform.position = Vector3.zero;
                    }
                }
            }
        }
    }

    public void ChangePos(Transform nextPos)
    {
        if (target!=player)
        {
            target = player;
        }
        else
        {
        target = nextPos;

        }

    }
    public void ToggleZoom()
    {
        zoomActivated = !zoomActivated;
    }
}
