﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryCamera : MonoBehaviour
{
    public GameObject target;
    public float offset = 1;
    public float speed = 10;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (target != null)
        {
            Vector3 newPos = target.transform.position + target.transform.forward * offset;
            transform.position = Vector3.MoveTowards(transform.position, newPos, speed * Time.deltaTime);
            transform.LookAt(target.transform.position, Vector3.up);
        }
        else
        {
            ChangeToCharacter();
        }
    }

    public void ChangeToPetTarget()
    {
        GetComponent<Camera>().cullingMask = (1 << LayerMask.NameToLayer("Pet"));
        target = PetManager.petManagerInstance.selectedPet.gameObject;
        transform.position = target.transform.position + target.transform.forward * offset;
    }

    public void ChangeToCharacter()
    {
        GetComponent<Camera>().cullingMask = (1 << LayerMask.NameToLayer("Player"));
        target = CharacterAttributeManager.characterAttributeManagerInstance.gameObject;
        transform.position = target.transform.position + target.transform.forward * offset;
    }
}
