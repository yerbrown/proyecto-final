﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterAttributeManager : MonoBehaviour
{
    public static CharacterAttributeManager characterAttributeManagerInstance;
    public Texture2D cursorArrow;
    [Header("Vida")]
    public int vida;
    public int vidaMax;
    public Image vidaImg;
    [Header("Hambre")]
    public float hambre;
    public float hambreMax;
    public float hambreTemp;
    public float hambreRest;
    public Image hambreImg;
    [Header("Sed")]
    public float sed;
    public float sedMax;
    public float sedTemp;
    public float sedRest;
    public Image sedImg;
    //Restablece todos los atributos del player y inicializa el sistema de la sed y el hambre.
    void Awake()
    {
        characterAttributeManagerInstance = this;
        vida = vidaMax;
        hambre = hambreMax;
        sed = sedMax;
        InvokeRepeating(nameof(RestarHamabre), hambreTemp, hambreTemp);
        InvokeRepeating(nameof(RestarSed), sedTemp, sedTemp);
    }
    //Resta el hambre del player y cuando llega a cero inicializa el sistema de perder vida.
    private void RestarHamabre()
    {

        hambre -= hambreRest;
        if (hambre < 0)
        {
            hambre = 0;
        }
        if (hambre == 0)
        {
            Invoke(nameof(RestarVida), 2);
        }
        ActualizarUI();
    }
    //Resta la sed del player y cuando llega a cero inicializa el sistema de perder vida.
    private void RestarSed()
    {
        sed -= sedRest;
        if (sed < 0)
        {
            sed = 0;
        }
        if (sed == 0)
        {
            Invoke(nameof(RestarVida), 2);
        }
        ActualizarUI();
    }
    //Resta la vida al player.
    private void RestarVida()
    {
        vida -= 2;
        ActualizarUI();
    }
    //Resta vida en base al daño recibido.
    public void RecibirDaño(int damageAmount)
    {
        vida -= damageAmount;
        ActualizarUI();
    }
    //Actualiza la interfaz de los atributos principales del player.
    public void ActualizarUI()
    {
        vidaImg.fillAmount = (float)vida / vidaMax;
        hambreImg.fillAmount = hambre / hambreMax;
        sedImg.fillAmount = sed / sedMax;
    }
    //Aumenta el hambre del Player en base al valor recibido.
    public void SumarHambre(int hambreSum)
    {
        hambre += hambreSum;
        if (hambre > hambreMax)
        {
            hambre = hambreMax;
        }
        ActualizarUI();
    }
    //Aumenta la sed del Player en base al valor recibido.
    public void SumarSed(int sedSum)
    {
        sed += sedSum;
        if (sed > sedMax)
        {
            sed = sedMax;
        }
        ActualizarUI();
    }
}