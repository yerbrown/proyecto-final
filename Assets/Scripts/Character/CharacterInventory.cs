﻿using System.Collections;
using System.Collections.Generic;
using UnityEditorInternal;
using UnityEngine;
using UnityEngine.UI;

public class CharacterInventory : MonoBehaviour
{
    public static CharacterInventory characterInventoryInstance;
    public Inventario inventory;
    public bool inWater = false;
    public QuickAccessButtonManager quickUIManager;
    // Declara el singleton
    private void Awake()
    {
        characterInventoryInstance = this;
    }
}
