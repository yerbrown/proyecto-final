﻿using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class CharacterMovement : MonoBehaviour
{

    public static CharacterMovement characterMovementInstance;
    public Camera cam;
    public float MovementRange, rotateSpeed;
    public float xAnim;
    public Animator animator;
    public NavMeshAgent agent;
    public bool canMove;
    public GameObject currentInteractiveObject;
    public GameObject selectedRoom;
    public GameObject containerPanel;
    public bool isInWatter;

    public CharacterSaveData charSaveData;

    private void Awake()
    {
        characterMovementInstance = this;
        ResetPos();
        SaveCharPetData();
        loadScenes.loadScenesInstance.saveData.AddListener(SaveCharPos);
    }

    public void SaveCharPetData()
    {
        if (charSaveData.foollowingPetKey != "")
        {
            PetInstantiateManager.instantiateManagerInstance.SearchPetByKey(charSaveData.foollowingPetKey).currentScene = SceneManager.GetActiveScene().buildIndex;
            PetInstantiateManager.instantiateManagerInstance.SearchPetByKey(charSaveData.foollowingPetKey).pos = transform.position + Vector3.back * 2;

        }

    }
    public void SaveCharPos()
    {
        for (int i = 0; i < charSaveData.allScenes.Count; i++)
        {
            if (charSaveData.allScenes[i].scene == SceneManager.GetActiveScene().buildIndex)
            {
                charSaveData.allScenes[i].pos = transform.position;
                charSaveData.allScenes[i].rot = transform.rotation;
                return;
            }
        }
    }
    public void ResetPos()
    {
        for (int i = 0; i < charSaveData.allScenes.Count; i++)
        {
            if (charSaveData.allScenes[i].scene == SceneManager.GetActiveScene().buildIndex)
            {
                transform.position = charSaveData.allScenes[i].pos;
                transform.rotation = charSaveData.allScenes[i].rot;
                return;
            }
        }
        charSaveData.allScenes.Add(new CharacterSaveData.PosInScene(SceneManager.GetActiveScene().buildIndex, transform.position, transform.rotation));
    }
    void Update()
    {
        MoveClicking();
        CharacterOpenContainer();
        SetAnimationValues();
        EnterRoom();
    }
    //Sistema para abrir containers
    private void CharacterOpenContainer()
    {
        if (transform.position.x == agent.destination.x && transform.position.z == agent.destination.z && currentInteractiveObject != null)
        {
            if (currentInteractiveObject.CompareTag("Chest"))
            {
                currentInteractiveObject.GetComponent<ContainerManager>().containerPanel = containerPanel;
                currentInteractiveObject.GetComponent<ContainerManager>().OpenContainer();
                transform.LookAt(new Vector3(currentInteractiveObject.transform.position.x, transform.position.y, currentInteractiveObject.transform.position.z));
                currentInteractiveObject = null;
                agent.velocity = Vector3.zero;
            }
        }
    }

    private void EnterRoom()
    {
        if (transform.position.x == agent.destination.x && transform.position.z == agent.destination.z)
        {
            if (selectedRoom != null)
            {
                PetInstantiateManager.instantiateManagerInstance.SavePetData();
                loadScenes.loadScenesInstance.loadSlider.transform.parent.gameObject.SetActive(true);
                loadScenes.loadScenesInstance.CargarPartida(selectedRoom.GetComponent<BuildingAttributes>().roomScene);
                selectedRoom = null;
            }
        }
    }
    //Sistema para moverte por el escenario
    public void MoveClicking()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (canMove)
            {
                Ray ray = cam.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit))
                {
                    selectedRoom = null;
                    if (!EventSystem.current.IsPointerOverGameObject())
                    {
                        if (hit.collider.CompareTag("Chest"))
                        {
                            currentInteractiveObject = hit.collider.gameObject;
                            agent.SetDestination(currentInteractiveObject.transform.GetChild(1).position);
                        }
                        else if (hit.collider.CompareTag("Pet"))
                        {

                        }
                        else if (hit.collider.CompareTag("Item"))
                        {
                            currentInteractiveObject = hit.collider.gameObject;
                            agent.SetDestination(hit.point);
                        }
                        else if (hit.collider.CompareTag("Floor"))
                        {
                            currentInteractiveObject = null;
                            agent.SetDestination(hit.point);
                        }
                        else if (hit.collider.CompareTag("Room"))
                        {
                            selectedRoom = hit.collider.gameObject;
                            agent.SetDestination(hit.collider.GetComponent<BuildingAttributes>().enterPos.position);
                        }
                        else
                        {

                        }
                    }

                }
            }
        }
        if (Input.GetMouseButton(0))
        {
            if (canMove)
            {
                Ray ray = cam.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit))
                {
                    if (!EventSystem.current.IsPointerOverGameObject())
                    {
                        if (hit.collider.CompareTag("Floor"))
                        {
                            currentInteractiveObject = null;
                            agent.SetDestination(hit.point);
                        }
                    }
                }
            }
        }
        float distance = Vector3.Distance(agent.destination, transform.position);
        if (distance < 0.1f)
        {
            transform.LookAt(new Vector3(agent.destination.x, transform.position.y, agent.destination.z));
        }
    }
    //Activa o desactiva el movimiento del player
    public void ToggleMovement(bool move)
    {
        if (move)
        {
            canMove = true;
        }
        else
        {
            canMove = false;
        }
        //agent.isStopped = !agent.isStopped;
        //canMove = !canMove;
    }

    public void EnableMovement()
    {
        canMove = true;
    }
    public void DisableMovement()
    {
        canMove = false;
    }
    //Vacia la variable del objeto que queria coger
    public void ResetCurrentItem()
    {
        currentInteractiveObject = null;
    }
    //Sistema de animaciones del PLayer
    public void SetAnimationValues()
    {
        animator.SetFloat("Forward", agent.velocity.magnitude / agent.speed);

        if (agent.isOnOffMeshLink)
        {
            animator.SetBool("Jump", true);

            float altura = Vector3.Dot(agent.currentOffMeshLinkData.endPos - transform.position, transform.up);

            animator.SetFloat("Distance", altura);
        }
        else
        {
            animator.SetBool("Jump", false);
        }
    }
    //Detecta con que objetos collisiona para recoger objetos
    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Water"))
        {
            agent.speed -= 2;
            isInWatter = true;
        }
        if (currentInteractiveObject != null)
        {
            if (other.CompareTag("Item") && currentInteractiveObject == other.gameObject)
            {
                CharacterInventory.characterInventoryInstance.inventory.AddItem(other.GetComponent<InteractiveActions>().itemStats, other.GetComponent<InteractiveActions>().amount);
                //CharacterInventory.characterInventoryInstance.quickUIManager.OrganizeButtons();
                other.GetComponent<InteractiveActions>().nameUI.gameObject.SetActive(false);
                Destroy(other.gameObject);

            }
        }
    }
    //Arregla un bug para cuando este encima de un objeto que quiere coger
    private void OnTriggerStay(Collider other)
    {
        if (currentInteractiveObject != null)
        {
            if (other.CompareTag("Item") && currentInteractiveObject == other.gameObject)
            {
                other.GetComponent<InteractiveActions>().nameUI.gameObject.SetActive(false);
                other.gameObject.SetActive(false);
            }
        }
    }
    //Detecta quue ha salido del agua para volver a la velocidad normal
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Water"))
        {
            agent.speed += 2;
            isInWatter = false;
        }
    }

}
