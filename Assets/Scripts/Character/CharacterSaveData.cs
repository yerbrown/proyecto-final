﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "CharSaveData", menuName = "ScriptableObjects/CharSaveDataScriptableObject", order = 9)]

public class CharacterSaveData : ScriptableObject
{
    public List<PosInScene> allScenes = new List<PosInScene>();
    [System.Serializable]
    public class PosInScene
    {
        public int scene;
        public Vector3 pos;
        public Quaternion rot;
        public PosInScene(int sceneNew, Vector3 posNew, Quaternion rotNew)
        {
            scene = sceneNew;
            pos = posNew;
            rot = rotNew;
        }
    }

    public string foollowingPetKey;
}
