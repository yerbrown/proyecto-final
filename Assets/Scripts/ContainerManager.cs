﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContainerManager : InteractiveActions
{
    public Inventario inventory;
    public GameObject containerPanel;
    public void OpenContainer()
    {
        containerPanel.GetComponent<InventoryButtonManager>().inventory = inventory;
        containerPanel.SetActive(true);
    }
}
