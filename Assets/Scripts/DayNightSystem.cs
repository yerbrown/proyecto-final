﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DayNightSystem : MonoBehaviour
{
    [SerializeField, Range(0, 24)]
    private float TimeOfDay;
    [SerializeField, Range(0, 1)]
    private float multiplicadorvelocidad;
    public Text clock;
    public GameObject lightSun, lightMoon;


    // Update is called once per frame
    void Update()
    {
        if (Application.isPlaying)
        {
            TimeOfDay += Time.deltaTime * multiplicadorvelocidad;
            if (TimeOfDay>=24)
            {
                TimeOfDay = 0;
            }
            //TimeOfDay %= 24;          
            clock.text = UpdateClock(TimeOfDay);
            lightSun.transform.Rotate(15 * Time.deltaTime * multiplicadorvelocidad, 0, 0 );
            lightMoon.transform.Rotate(15 * Time.deltaTime * multiplicadorvelocidad, 0, 0);
        }
    }

    public void UpdateLighting()
    {

    }

    public string UpdateClock(float elapsed)
    {

        int hours = (int)elapsed;
        int minutes = (int)((elapsed-hours)*60.0f);
        return String.Format("{0:00}:{1:00}", hours, minutes);
    }
}
