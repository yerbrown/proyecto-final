﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BuildingAttributes : MonoBehaviour
{
    public Transform enterPos;
    public int roomScene;
    public Texture2D cursorArrow, cursorDoorArrow;
    private void OnMouseEnter()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            Cursor.SetCursor(cursorDoorArrow, Vector2.zero, CursorMode.ForceSoftware);
            ControllersHelper.controllersHelperInstance.ToogleImage(true);
            ControllersHelper.controllersHelperInstance.ChangeKey(gameObject);
        }
    }

    private void OnMouseExit()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            Cursor.SetCursor(cursorArrow, Vector2.zero, CursorMode.ForceSoftware);
            ControllersHelper.controllersHelperInstance.ToogleImage(false);
        }
    }
}