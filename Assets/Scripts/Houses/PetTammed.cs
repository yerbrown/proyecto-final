﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
[CreateAssetMenu(fileName = "PetList", menuName = "ScriptableObjects/PetListScriptableObject", order = 8)]
public class PetTammed : ScriptableObject
{
    public List<PetAll> allPets = new List<PetAll>();
    public int numPets;
    [System.Serializable]
    public class PetAll 
    {
        public PetAttributes petRaceInfo;
        public string petName;
        public int life;
        public int lifeMax;
        public float attackRate;
        public float lootRate;
        public int damage;
        public Inventario inventory;
        public Inventario.Item armor;
        public Inventario.Item item;
        public Inventario.Item food;

        public Vector3 pos;
        public Quaternion rot;
        public int currentScene;
        public string petKey;

        public int currentExperience, experienceToLvlUp;
        public int level;
        public bool inStable;
        public PetAll(AnimalsController animal, ref List<PetAll> listPets, ref int numPets)
        {
            numPets++;
            petKey = "Pet_" + numPets;
            petRaceInfo = animal.petStats;
            petName = animal.petName;
            life = animal.life;
            lifeMax = animal.lifeMax;
            attackRate = animal.attackRate;
            lootRate = animal.lootRate;
            damage = animal.damage;
            inventory = animal.inventory;
            armor = animal.armor;
            item = animal.item;
            food = animal.food;

            pos = animal.transform.position;
            rot = animal.transform.rotation;
            currentScene = SceneManager.GetActiveScene().buildIndex;

            currentExperience = animal.GetComponent<PetExperienceController>().currentExperience;
            experienceToLvlUp = animal.GetComponent<PetExperienceController>().experienceToLvlUp;
            level = animal.GetComponent<PetExperienceController>().level;
            listPets.Add(this);
        }
    }

}
