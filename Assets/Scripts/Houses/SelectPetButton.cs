﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectPetButton : MonoBehaviour
{
    public PetTammed.PetAll currentPet;
    private void Start()
    {
        transform.GetChild(0).GetComponent<Image>().sprite = currentPet.petRaceInfo.iconSprite;
    }
    public void SelectPetInStable()
    {
        StableManager.stableManagerInstance.SelectPetInStable(currentPet);
    }
}
