﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;

public class StableManager : MonoBehaviour
{
    public static StableManager stableManagerInstance;


    public PetTammed myPets;
    public PetTammed.PetAll selectedPet;
    public List<GameObject> petPrefabs = new List<GameObject>();

    public GameObject panelEstable;
    public GameObject prefabButton;
    public GameObject menu;
    public Image currentPetImg, petSelectedImg;
    public Text currentPetName, currentPetLevel;
    public Text selectedPetName, selectedPetLevel, selectedPetExp;
    public Image selectedPetExpImg;

    public List<GameObject> allButtons = new List<GameObject>();

    public Transform limits;

    private void Awake()
    {
        stableManagerInstance = this;
    }
    private void Start()
    {
        for (int i = 0; i < myPets.allPets.Count; i++)
        {
            if (myPets.allPets[i].inStable == true)
            {
                InstantiateInStable(myPets.allPets[i]);

            }
        }
        ResetStablePanel();
    }


    public void InstantiateInStable(PetTammed.PetAll pet)
    {
        GameObject newPetInStable = Instantiate(SearchPetPrefab(pet), limits.position + new Vector3(Random.Range(-2f, 2f), 0, Random.Range(-4f, 4f)), Quaternion.Euler(0, 0, 0));
        PetInstantiateManager.instantiateManagerInstance.ResetPetData(newPetInStable.GetComponent<AnimalsController>(), pet);
        PetInstantiateManager.instantiateManagerInstance.inScenePets.Add(newPetInStable.GetComponent<AnimalsController>());
        newPetInStable.tag = "StablePet";
    }
    public void ShowCurrentPetFollow()
    {
        if (CharacterMovement.characterMovementInstance.charSaveData.foollowingPetKey != "")
        {
            for (int i = 0; i < myPets.allPets.Count; i++)
            {
                if (myPets.allPets[i].petKey == CharacterMovement.characterMovementInstance.charSaveData.foollowingPetKey)
                {
                    currentPetImg.sprite = myPets.allPets[i].petRaceInfo.iconSprite;
                    currentPetLevel.text = "Lvl: " + myPets.allPets[i].level;
                    currentPetName.text = myPets.allPets[i].petName;
                    return;
                }
            }
        }
        else
        {
            currentPetImg.sprite = null;
            currentPetLevel.text = "Lvl: 0";
            currentPetName.text = "Pet Name";
        }
    }

    public void SelectPetInStable(PetTammed.PetAll pet)
    {
        selectedPet = pet;
        selectedPetExp.text = pet.currentExperience + "/" + pet.experienceToLvlUp;
        selectedPetExpImg.fillAmount = (float)pet.currentExperience / pet.experienceToLvlUp;
        selectedPetName.text = pet.petName;
        selectedPetLevel.text = "Lvl: " + pet.level;
        petSelectedImg.sprite = pet.petRaceInfo.iconSprite;
    }

    public void ChangePet()
    {
        if (selectedPet.petKey != CharacterMovement.characterMovementInstance.charSaveData.foollowingPetKey)
        {
            DropInStable();
        }
        CharacterMovement.characterMovementInstance.charSaveData.foollowingPetKey = selectedPet.petKey;
        for (int i = 0; i < myPets.allPets.Count; i++)
        {
            if (myPets.allPets[i].petKey == selectedPet.petKey)
            {
                for (int j = 0; j < PetInstantiateManager.instantiateManagerInstance.inScenePets.Count; j++)
                {
                    if (PetInstantiateManager.instantiateManagerInstance.inScenePets[j].animalKey == selectedPet.petKey)
                    {
                        Destroy(PetInstantiateManager.instantiateManagerInstance.inScenePets[j].gameObject);
                        PetInstantiateManager.instantiateManagerInstance.inScenePets.Remove(PetInstantiateManager.instantiateManagerInstance.inScenePets[j]);
                        break;
                    }
                }
                myPets.allPets[i].inStable = false;
                break;
            }
        }
        CharacterMovement.characterMovementInstance.SaveCharPetData();
        PetInstantiateManager.instantiateManagerInstance.InstatiatePet(selectedPet);
        PetInstantiateManager.instantiateManagerInstance.InstantiateCharPetFollow();
        ShowCurrentPetFollow();
        ResetSelectedPetPanel();
        ResetStablePanel();

    }

    public void DropInStable()
    {
        if (CharacterMovement.characterMovementInstance.charSaveData.foollowingPetKey != "")
        {
            for (int i = 0; i < myPets.allPets.Count; i++)
            {
                if (CharacterMovement.characterMovementInstance.charSaveData.foollowingPetKey == myPets.allPets[i].petKey)
                {
                    myPets.allPets[i].inStable = true;
                    for (int j = 0; j < PetInstantiateManager.instantiateManagerInstance.inScenePets.Count; j++)

                        if (PetInstantiateManager.instantiateManagerInstance.inScenePets[j].animalKey == CharacterMovement.characterMovementInstance.charSaveData.foollowingPetKey)
                        {
                            PetManager.petManagerInstance.followTog.isOn = false;
                            Destroy(PetInstantiateManager.instantiateManagerInstance.inScenePets[j].gameObject);
                            PetInstantiateManager.instantiateManagerInstance.inScenePets.Remove(PetInstantiateManager.instantiateManagerInstance.inScenePets[j]);
                            InstantiateInStable(myPets.allPets[i]);
                            break;
                        }
                }
            }
            ShowCurrentPetFollow();
            ResetStablePanel();
        }
    }

    public GameObject SearchPetPrefab(PetTammed.PetAll petSearch)
    {
        for (int i = 0; i < petPrefabs.Count; i++)
        {
            if (petPrefabs[i].GetComponent<AnimalsController>().petStats.raceName == petSearch.petRaceInfo.raceName)
            {
                return petPrefabs[i];
            }
        }
        return null;
    }
    public void ResetSelectedPetPanel()
    {
        selectedPet = null;
        selectedPetExp.text = "0/0";
        selectedPetExpImg.fillAmount = 0;
        selectedPetName.text = "Pet Name";
        selectedPetLevel.text = "Lvl: 0";
        petSelectedImg.sprite = null;
    }
    public void GenerateButtons()
    {
        for (int i = 0; i < myPets.allPets.Count; i++)
        {
            if (myPets.allPets[i].inStable == true)
            {
                GameObject newButton = Instantiate(prefabButton, panelEstable.transform);
                newButton.GetComponent<SelectPetButton>().currentPet = myPets.allPets[i];
                allButtons.Add(newButton);
            }
        }
    }

    public void DeleteAllButtons()
    {
        for (int i = allButtons.Count - 1; i >= 0; i--)
        {
            Destroy(allButtons[i]);
            allButtons.Remove(allButtons[i]);
        }
    }

    public void ResetStablePanel()
    {
        DeleteAllButtons();
        GenerateButtons();
    }

    public void ToggleMenu()
    {
        menu.SetActive(!menu.activeSelf);
    }
}
