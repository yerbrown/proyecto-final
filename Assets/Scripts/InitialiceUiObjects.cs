﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitialiceUiObjects : MonoBehaviour
{
    public List<GameObject> uiObjects;
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < uiObjects.Count; i++)
        {
            uiObjects[i].SetActive(false);
        }
    }


}
