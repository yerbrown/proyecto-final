﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InteractiveActions : MonoBehaviour
{
    public ItemInventario itemStats;
    public Text nameUI;
    public Transform posTrans, namePos;
    public int amount = 1;
    public ObjectType objectType;
    public enum ObjectType
    {
        Comida,
        Bebida,
        Recipiente_Liquido,
        Contenedor
    }
    private void Awake()
    {
        nameUI = GameObject.Find("SelectedName").GetComponent<Text>();
    }
    private void OnMouseEnter()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            Cursor.SetCursor(itemStats.cursorItemArrow, Vector2.zero, CursorMode.ForceSoftware);
            nameUI.GetComponent<LenguageChanger>().keyWord = itemStats.itemName;
            if (amount > 1)
            {
                nameUI.GetComponent<LenguageChanger>().amount = amount;
            }
            if (nameUI.text=="")
            {
                nameUI.GetComponent<LenguageChanger>().Traduct();
            }
            nameUI.gameObject.SetActive(true);

            GetComponent<Outline>().enabled = true;
            if (PetManager.petManagerInstance.currentPet == null || PetManager.petManagerInstance.currentPetState == AnimalsController.Mode.Follow)
            {
                ControllersHelper.controllersHelperInstance.ToogleImage(true);
                ControllersHelper.controllersHelperInstance.ChangeKey(gameObject);
            }
        }
    }
    private void OnMouseOver()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            if (!nameUI.gameObject.activeInHierarchy)
            {
                nameUI.gameObject.SetActive(true);
                if (itemStats == null)
                {
                    nameUI.text = gameObject.name;
                }
                else
                {
                    nameUI.text = itemStats.itemName;
                }
                GetComponent<Outline>().enabled = true;
            }
        }
        ClampName();
    }
    private void OnMouseExit()
    {
        Cursor.SetCursor(CharacterAttributeManager.characterAttributeManagerInstance.cursorArrow, Vector2.zero, CursorMode.ForceSoftware);
        nameUI.gameObject.SetActive(false);
        GetComponent<Outline>().enabled = false;
        ControllersHelper.controllersHelperInstance.ToogleImage(false);
    }
    public void ClampName()
    {
        Vector3 nameUIPos = Camera.main.WorldToScreenPoint(namePos.position);
        nameUI.transform.position = nameUIPos;
    }
}
