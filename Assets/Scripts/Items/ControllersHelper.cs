﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControllersHelper : MonoBehaviour
{
    public static ControllersHelper controllersHelperInstance;
    public Vector2 mousePosOffset;
    public List<Sprite> controllSprites = new List<Sprite>();
    private void Awake()
    {
        controllersHelperInstance = this;
        ToogleImage(false);
    }


    // Update is called once per frame
    void Update()
    {
        transform.position = Input.mousePosition + (Vector3)mousePosOffset;
    }

    public void ChangeKey(GameObject itemToInteract)
    {
        if (TryGetComponent(out Image spriteImage))
        {
            if (itemToInteract.CompareTag("CharPet"))
            {
                spriteImage.sprite = controllSprites[2];
            }
            else if (itemToInteract.CompareTag("Item") || itemToInteract.CompareTag("Chest") || itemToInteract.CompareTag("WildPet")|| itemToInteract.CompareTag("Room"))
            {
                spriteImage.sprite = controllSprites[1];
            }
            else if (itemToInteract.CompareTag("Resource"))
            {
                spriteImage.sprite = controllSprites[1];
            }
        }
    }

    public void ToogleImage(bool enabled)
    {
        GetComponent<Image>().enabled = enabled;
    }

}
