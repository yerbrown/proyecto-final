﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Inventory", menuName = "ScriptableObjects/InventarioScriptableObject", order = 3)]
public class Inventario : ScriptableObject
{
    public List<Item> itemsInInventory = new List<Item>();

    [System.Serializable]
    public class Item
    {
        public ItemInventario objeto;
        public int amount;
        public bool quickAccess;
        public int quickPos;

        public Item()
        {

        }

        public Item(ItemInventario objetoN, int amountN)
        {
            objeto = objetoN;
            amount = amountN;
        }
        public Item(ItemConsumible objetoN, int amountN)
        {
            objeto = objetoN;
            amount = amountN;
        }
    }
    //Añade una cantidad establecida de un objeto al inventario
    public void AddItem(ItemInventario item, int amount = 1)
    {
        if (item.stackable)
        {
            for (int i = 0; i < itemsInInventory.Count; i++)
            {
                if (item == itemsInInventory[i].objeto)
                {
                    if (itemsInInventory[i].objeto.stackAmount > itemsInInventory[i].amount)
                    {
                        itemsInInventory[i].amount += amount;
                        if (itemsInInventory[i].amount > itemsInInventory[i].objeto.stackAmount)
                        {
                            int rest = itemsInInventory[i].amount - itemsInInventory[i].objeto.stackAmount;
                            itemsInInventory[i].amount = itemsInInventory[i].objeto.stackAmount;
                            amount = rest;
                        }
                        else
                        {
                            QuickAccessButtonManager.quickAccessButtonManagerInstance.OrganizeButtons();
                            return;
                        }

                    }
                }
            }
        }
        itemsInInventory.Add(new Item(item, amount));
        QuickAccessButtonManager.quickAccessButtonManagerInstance.OrganizeButtons();
    }

    public void ItemBack(Item item)
    {
        itemsInInventory.Remove(item);
        itemsInInventory.Add(item);
    }
}
