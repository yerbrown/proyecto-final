﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class InventoryButtonManager : MonoBehaviour
{
    [System.Serializable]
    public class ItemList
    {
        public string itemTypeName;
        public List<Inventario.Item> listOfItems = new List<Inventario.Item>();

        public ItemList(Inventario.Item item)
        {
            itemTypeName = item.objeto.itemName;
            listOfItems.Add(item);
        }
    }
    public void AddItem(Inventario.Item item)
    {

    }
    public Inventario inventory;
    public List<Inventario.Item> sortedObjects = new List<Inventario.Item>();
    public List<ItemList> listObjType = new List<ItemList>();
    public InventoryButtonManager nextInventoryManager;
    public List<Button> buttons = new List<Button>();
    public QuickAccessButtonManager quickAccess;
    public GameObject standarButton, useButton, containerButton, petButton, petCharButton;



    private void OnEnable()
    {
        SortInventoryItem();
    }
    //Reorganiza los botones del inventario
    public void OrganizeButtons()
    {
        DeleteButtons();
        int buttonPos = 0;
        for (int i = 0; i < buttons.Count; i++)
        {
            if (i < inventory.itemsInInventory.Count)
            {
                GameObject newButton = null;

                if (inventory.name == "Character" && inventory.itemsInInventory[i].quickAccess)
                {
                    continue;
                }
                if (containerButton != null)
                {
                    newButton = Instantiate(containerButton, buttons[buttonPos].transform);
                }
                else if (petButton != null)
                {
                    newButton = Instantiate(petButton, buttons[buttonPos].transform);
                    newButton.GetComponent<ToCharacterFromPetButton>().characterInventoryManager = nextInventoryManager;
                }
                else if (petCharButton != null)
                {
                    newButton = Instantiate(petCharButton, buttons[buttonPos].transform);
                    newButton.GetComponent<ToPetFromCharacterButton>().petInventoryManager = nextInventoryManager;
                }
                else
                {
                    if (inventory.itemsInInventory[i].objeto.tipo == ItemInventario.ItemType.Basico|| inventory.itemsInInventory[i].objeto.tipo == ItemInventario.ItemType.Equipo|| inventory.itemsInInventory[i].objeto.tipo == ItemInventario.ItemType.Arma)
                    {
                        newButton = Instantiate(standarButton, buttons[buttonPos].transform);
                    }
                    else if (inventory.itemsInInventory[i].objeto.tipo == ItemInventario.ItemType.Consumible)
                    {
                        newButton = Instantiate(useButton, buttons[buttonPos].transform);
                    }
                }
                ItemButton boton = newButton.GetComponent<ItemButton>();
                boton.inventory = inventory;
                boton.item = inventory.itemsInInventory[i];
                boton.transform.GetChild(1).GetComponent<LenguageChanger>().keyWord = boton.item.objeto.itemName;
                boton.inventoryManager = this;
                if (boton.GetComponent<ItemDragHandler>() != null)
                {
                    boton.GetComponent<ItemDragHandler>().manager = this;
                }
                boton.UpdateButtonUI();
                buttonPos++;
            }
        }
    }

    public void SortInventoryItem()
    {
        CreateListOfListItem();
        bool added;
        for (int i = 0; i < listObjType.Count; i++)
        {
            added = false;
            for (int j = 0; j < sortedObjects.Count; j++)
            {
                if (listObjType[i].itemTypeName.CompareTo(sortedObjects[j].objeto.itemName) < 0)
                {
                    for (int k = listObjType[i].listOfItems.Count - 1; k >= 0; k--)
                    {
                        sortedObjects.Insert(j, listObjType[i].listOfItems[k]);
                    }
                    added = true;
                    break;
                }
            }

            if (!added)
            {
                for (int j = listObjType[i].listOfItems.Count - 1; j >= 0; j--)
                {
                    sortedObjects.Add(listObjType[i].listOfItems[j]);
                }
            }
        }
        ReorganizeInventory();
        if (quickAccess != null)
        {
            quickAccess.OrganizeButtons();
        }
    }

    static int SortByAmount(Inventario.Item item1, Inventario.Item item2)
    {
        return item1.amount.CompareTo(item2.amount);
    }

    public void ReorganizeInventory()
    {
        for (int i = inventory.itemsInInventory.Count - 1; i >= 0; i--)
        {
            inventory.itemsInInventory.Remove(inventory.itemsInInventory[i]);
        }
        for (int i = 0; i < sortedObjects.Count; i++)
        {
            inventory.itemsInInventory.Add(sortedObjects[i]);
        }
        for (int i = sortedObjects.Count - 1; i >= 0; i--)
        {
            sortedObjects.Remove(sortedObjects[i]);
        }
        OrganizeButtons();
    }
    //Borra todos los botones
    public void DeleteButtons()
    {
        for (int i = 0; i < buttons.Count; i++)
        {
            if (buttons[i].transform.childCount > 0)
            {
                for (int j = 0; j < buttons[i].transform.childCount; j++)
                {
                    Destroy(buttons[i].transform.GetChild(j).gameObject);
                }
            }
        }
    }

    private bool ComprobarQuickAccess(Inventario.Item compitem)
    {
        foreach (Button itemButton in quickAccess.quickAccess)
        {
            if (itemButton.transform.GetChild(0).GetComponent<ItemButton>() != null && itemButton.transform.GetChild(0).GetComponent<ItemButton>().item == compitem)
            {
                compitem.quickAccess = true;
                compitem.quickPos = int.Parse(itemButton.transform.GetChild(1).GetComponent<Text>().text) - 1;
                return true;
            }
        }
        return false;
    }

    public void CreateListOfListItem()
    {
        for (int i = listObjType.Count - 1; i >= 0; i--)
        {
            listObjType.Remove(listObjType[i]);
        }
        for (int i = 0; i < inventory.itemsInInventory.Count; i++)
        {
            bool added = false;
            for (int j = 0; j < listObjType.Count; j++)
            {
                if (inventory.itemsInInventory[i].objeto.itemName == listObjType[j].itemTypeName)
                {
                    listObjType[j].listOfItems.Add(inventory.itemsInInventory[i]);
                    listObjType[j].listOfItems.Sort(SortByAmount);
                    added = true;
                    break;
                }
            }
            if (!added)
            {
                listObjType.Add(new ItemList(inventory.itemsInInventory[i]));
            }
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.U))
        {
            CreateListOfListItem();
        }
    }
}
