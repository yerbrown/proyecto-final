﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "ItemConsumible", menuName = "ScriptableObjects/ItemConsumibleScriptableObject", order = 5)]
public class ItemConsumible : ItemInventario
{
    public int vidaSum;
    public int hambreSum;
    public int sedSum;
    public AudioClip audioClip;
    //Aumenta los atributos en base a sus estadisticas
    public override void Consume()
    {
        CharacterAttributeManager.characterAttributeManagerInstance.vida += vidaSum;
        if (CharacterAttributeManager.characterAttributeManagerInstance.vida > CharacterAttributeManager.characterAttributeManagerInstance.vidaMax)
        {
            CharacterAttributeManager.characterAttributeManagerInstance.vida = CharacterAttributeManager.characterAttributeManagerInstance.vidaMax;
        }
        CharacterAttributeManager.characterAttributeManagerInstance.hambre += hambreSum;
        if (CharacterAttributeManager.characterAttributeManagerInstance.hambre > CharacterAttributeManager.characterAttributeManagerInstance.hambreMax)
        {
            CharacterAttributeManager.characterAttributeManagerInstance.hambre = CharacterAttributeManager.characterAttributeManagerInstance.hambreMax;
        }
        CharacterAttributeManager.characterAttributeManagerInstance.sed += sedSum;
        if (CharacterAttributeManager.characterAttributeManagerInstance.sed > CharacterAttributeManager.characterAttributeManagerInstance.sedMax)
        {
            CharacterAttributeManager.characterAttributeManagerInstance.sed = CharacterAttributeManager.characterAttributeManagerInstance.sedMax;
        }

        CharacterAttributeManager.characterAttributeManagerInstance.GetComponent<CharacterMovement>().animator.SetTrigger("Eat");
        AudioManager.audioSourceInstance.audioSource.PlayOneShot(audioClip);
    }

    public override ItemConsumible ObtainConsumible()
    {
        return this;
    }
}
