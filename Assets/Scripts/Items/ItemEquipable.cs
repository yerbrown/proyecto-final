﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "ItemConsumible", menuName = "ScriptableObjects/ItemEquipableScriptableObject", order = 7)]
public class ItemEquipable : ItemInventario
{
    public int damage;
    public int defense;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override ItemEquipable ObtainEquipable()
    {
        return this;
    }
}
