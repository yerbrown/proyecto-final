﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ItemInfoViewer : MonoBehaviour
{
    public static ItemInfoViewer itemInfoViewerInstance;
    [Header("ui")]
    public float offset = 2;
    public GameObject basic;
    public GameObject armor;
    public GameObject weapon;
    public GameObject food;
    public Text itemName;
    [Header("Basic")]
    public Text itemInfo;
    [Header("Armor")]
    public Text armorInfo;
    public Text weaponInfo;
    public Text damageTxt, armorTxt;
    [Header("Food")]
    public Text foodInfo;
    public Text lifeTxt, hungryTxt, thirstTxt;
    private void Awake()
    {
        itemInfoViewerInstance = this;
    }
    public void UpdateUI(Inventario.Item item, Vector3 pos)
    {
        itemName.text = item.objeto.itemName;
        if (item.objeto.tipo == ItemInventario.ItemType.Basico)
        {
            basic.SetActive(true);
            armor.SetActive(false);
            food.SetActive(false);
            weapon.SetActive(false);
            itemInfo.text = item.objeto.itemDescription;
        }
        else if (item.objeto.tipo == ItemInventario.ItemType.Consumible)
        {
            basic.SetActive(false);
            armor.SetActive(false);
            food.SetActive(true);
            weapon.SetActive(false);
            foodInfo.text = item.objeto.itemDescription;

            lifeTxt.text = item.objeto.ObtainConsumible().vidaSum.ToString("00");
            hungryTxt.text = item.objeto.ObtainConsumible().hambreSum.ToString("00");
            thirstTxt.text = item.objeto.ObtainConsumible().sedSum.ToString("00");
        }
        else if (item.objeto.tipo == ItemInventario.ItemType.Arma)
        {
            basic.SetActive(false);
            armor.SetActive(false);
            food.SetActive(false);
            weapon.SetActive(true);
            weaponInfo.text = item.objeto.itemDescription;

            damageTxt.text = item.objeto.ObtainEquipable().damage.ToString("00");
        }
        else
        {
            basic.SetActive(false);
            armor.SetActive(true);
            food.SetActive(false);
            weapon.SetActive(false);
            armorInfo.text = item.objeto.itemDescription;


            armorTxt.text = item.objeto.ObtainEquipable().defense.ToString("00");
        }
        if (pos.y > 540)
        {
            transform.position = pos + Vector3.down * offset;

        }
        else
        {
            transform.position = pos + Vector3.up * offset;

        }

        //if (pos.x < 960 - 743)
        //{
        //    transform.position = new Vector3(960 - 743, transform.position.y, transform.position.z);
        //}else if (pos.x > 960 + 743)
        //{
        //    transform.position = new Vector3(960 + 743, transform.position.y, transform.position.z);
        //}
    }

}
