﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Item", menuName = "ScriptableObjects/ItemBaseScriptableObject", order = 4)]
public class ItemInventario : ScriptableObject
{
    public string itemName;
    public Sprite itemSpr;
    public bool stackable;
    public int stackAmount = 10;
    public Texture2D cursorItemArrow;
    public ItemType tipo;
    public string itemDescription;
    //Tipo de consumible
    public enum ItemType
    {
        Basico,
        Consumible,
        Equipo,
        Arma
    }
    public virtual void Consume()
    {

    }

    public virtual ItemEquipable ObtainEquipable()
    {
        return null;
    }
    public virtual ItemConsumible ObtainConsumible()
    {
        return null;
    }
}
