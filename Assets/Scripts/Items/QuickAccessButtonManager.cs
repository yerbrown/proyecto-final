﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuickAccessButtonManager : MonoBehaviour
{
    public static QuickAccessButtonManager quickAccessButtonManagerInstance;
    public Inventario inventory;
    public InventoryButtonManager inventoryManager;
    public List<Button> quickAccess = new List<Button>();
    public GameObject standarButton, useButton;
    private void Awake()
    {
        quickAccessButtonManagerInstance = this;
    }

    private void Start()
    {
        OrganizeButtons();
    }
    private void Update()
    {
        if (Input.anyKeyDown)
        {
            string input = Input.inputString;
            if (int.TryParse(input, out int number) && number > 0 && number < 10)
            {
                UseButton(number);
            }
        }
    }

    public void UseButton(int number)
    {
        if (quickAccess[number - 1].transform.GetChild(0).TryGetComponent(out Button boton))
        {
            boton.onClick.Invoke();
        }
    }
    public void OrganizeButtons()
    {
        DeleteButtons();
        for (int i = 0; i < inventory.itemsInInventory.Count; i++)
        {
            if (inventory.itemsInInventory[i].quickAccess)
            {
                GameObject newButton = null;
                if (inventory.itemsInInventory[i].objeto.tipo == ItemInventario.ItemType.Basico|| inventory.itemsInInventory[i].objeto.tipo == ItemInventario.ItemType.Equipo)
                {
                    newButton = Instantiate(standarButton, quickAccess[inventory.itemsInInventory[i].quickPos].transform);
                }
                else if (inventory.itemsInInventory[i].objeto.tipo == ItemInventario.ItemType.Consumible)
                {
                    newButton = Instantiate(useButton, quickAccess[inventory.itemsInInventory[i].quickPos].transform);
                }
                newButton.transform.SetAsFirstSibling();
                ItemButton boton = newButton.GetComponent<ItemButton>();
                boton.inventory = inventory;
                boton.inventoryManager = inventoryManager;
                boton.item = inventory.itemsInInventory[i];
                boton.transform.GetChild(1).GetComponent<LenguageChanger>().keyWord = boton.item.objeto.itemName;
                if (boton.GetComponent<ItemDragHandler>() != null)
                {
                    boton.GetComponent<ItemDragHandler>().manager = inventoryManager;
                }
                boton.UpdateButtonUI();
            }
        }
    }

    public void DeleteButtons()
    {
        for (int i = 0; i < quickAccess.Count; i++)
        {
            for (int j = 0; j < quickAccess[i].transform.childCount; j++)
            {
                if (quickAccess[i].transform.GetChild(j).GetComponent<ItemDragHandler>() != null)
                {
                    Destroy(quickAccess[i].transform.GetChild(j).gameObject);
                }
            }
        }
    }
}

