﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PetMenuToggle : MonoBehaviour
{
    public Button openInventory;
    public void ToggleMenuName()
    {
        if (PetManager.petManagerInstance.currentPet != null)
        {
            PetManager.petManagerInstance.selectedPet = PetManager.petManagerInstance.currentPet;
            PetManager.petManagerInstance.ShowName(true);
        }
        else
        {
            if (!PetManager.petManagerInstance.petMenu.activeSelf)
            {
                PetManager.petManagerInstance.selectedPet = null;
                PetManager.petManagerInstance.ShowName(false);
            }
        }
    }

    private void Update()
    {
        if (gameObject.activeSelf)
        {
            if (Vector3.Distance(CharacterInventory.characterInventoryInstance.gameObject.transform.position, PetManager.petManagerInstance.selectedPet.transform.position) <= 5)
            {
                openInventory.interactable = true;
            }
            else
            {
                openInventory.interactable = false;   
                
            }
        }
    }
}
