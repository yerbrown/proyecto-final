﻿using TMPro;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
public class AnimalsController : MonoBehaviour
{
    //[HideInInspector]
    public string animalKey;
    [Header("UI")]
    public Transform nameTrans;
    [Header("Atributes")]
    public PetAttributes petStats;
    public string petName;
    public int life;
    public int lifeMax;
    public float attackRate;
    public float lootRate;
    public int damage;
    public bool moveing;
    public Inventario inventory;
    [Header("Properties")]
    public NavMeshAgent agent;
    public Animator anim;
    public Camera cam;
    public Mode modo = 0;
    public int moveRange = 10;
    public float stopDistance = 1.5f;
    [Header("Targets")]
    public Transform player;
    public bool followingPlayer;
    [Header("Equipment")]
    public Inventario.Item armor = null;
    public Inventario.Item item = null;
    public Inventario.Item food = null;

    public ResourceContainer resourceSource;
    public enum Mode
    {
        StandBy,
        Follow,
        MoveControl,
        GoToResource,
        Loot,
        Combat
    }
    //Le pasa las estadisticas de la raza a esta mascota
    private void Awake()
    {
        cam = Camera.main;
        petName = petStats.raceName;
        life = petStats.lifeMax;
        lifeMax = petStats.lifeMax;
        attackRate = petStats.attackRate;
        lootRate = petStats.lootRate;
        damage = petStats.damageBase;
        InvokeRepeating(nameof(CureTime), 1, 2);
    }
    void Update()
    {
        switch (modo)
        {
            case Mode.StandBy:
                if (moveing == false)
                {
                    moveing = true;
                    Invoke(nameof(RandomChangePosition), 5);
                }
                break;
            case Mode.Follow:
                FollowPlayer();
                break;
            case Mode.MoveControl:
                MoveClicking();
                break;
            case Mode.GoToResource:
                DetectResource();
                ChangeLootTarget();
                break;
            case Mode.Loot:
                ChangeLootTarget();
                break;
            case Mode.Combat:
                break;
            default:
                break;
        }
        ChangeToCombatMode();
        anim.SetFloat("Forward", agent.velocity.magnitude / agent.speed);
        if (true)
        {

        }
    }

    public void ChangeToCombatMode()
    {
        if (Input.GetMouseButtonDown(0) && followingPlayer)
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (!EventSystem.current.IsPointerOverGameObject())
                {
                    if (PetManager.petManagerInstance.currentPetIcon.gameObject.activeSelf)
                    {
                        if (Vector3.Distance(hit.point, player.position) < moveRange)
                        {
                            if (hit.collider.CompareTag("WildPet"))
                            {
                                if (resourceSource != null)
                                {
                                    resourceSource.manager.currentLootSource = null;
                                    resourceSource.manager.ShowName(false);
                                    resourceSource = null;
                                    CancelInvoke(nameof(Loot));
                                }
                                GetComponent<CombatController>().enemyTarget = hit.collider.transform;
                                WildPetManager.instanceWildPetManager.currentWildPet = hit.collider.GetComponent<WildPetController>();
                                WildPetManager.instanceWildPetManager.ShowName(true);
                                modo = Mode.Combat;
                            }
                        }
                    }
                }
            }
        }
    }


    //Sigue al player
    public void FollowPlayer()
    {
        agent.SetDestination(player.position);
        agent.stoppingDistance = 2;
    }
    //Detecta el recurso y cmbia al modo loot
    public void DetectResource()
    {
        if (resourceSource != null)
        {
            if (modo != Mode.Loot)
            {
                //*if (Vector3.Distance(resourceSource.transform.position, transform.position) <= stopDistance)

                if (agent.remainingDistance == 0)
                {
                    agent.stoppingDistance = stopDistance;
                    transform.LookAt(resourceSource.transform.position);
                    InvokeRepeating(nameof(Loot), 0, lootRate);
                    modo = Mode.Loot;
                }
            }
        }
    }
    //Si estas looteando y cambia de fuente de recursos
    public void ChangeLootTarget()
    {
        if (Input.GetMouseButtonDown(0) && followingPlayer)
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (!EventSystem.current.IsPointerOverGameObject())
                {
                    if (PetManager.petManagerInstance.currentPetIcon.gameObject.activeSelf)
                    {
                        if (Vector3.Distance(hit.point, player.position) < moveRange)
                        {
                            if (hit.collider.CompareTag("Floor"))
                            {
                                resourceSource.manager.currentLootSource = null;
                                resourceSource.manager.ShowName(false);
                                resourceSource = null;
                                agent.SetDestination(hit.point);
                                CancelInvoke(nameof(Loot));
                                modo = Mode.MoveControl;
                            }
                            else if (hit.collider.GetComponent<ResourceContainer>() != resourceSource && hit.collider.GetComponent<ResourceContainer>() != null && hit.collider.GetComponent<ResourceContainer>().resource.resourceType == petStats.looterType)
                            {
                                modo = Mode.GoToResource;

                                SelectedResource(hit);
                            }
                        }
                    }
                }
            }
        }
        float distance = Vector3.Distance(agent.destination, transform.position);
        if (distance < 0.1f)
        {
            transform.LookAt(new Vector3(agent.destination.x, transform.position.y, agent.destination.z));
        }
    }

    //Detecta el tipo de recurso que recoge
    public virtual void SelectedResource(RaycastHit hit)
    {

    }
    //Mueve a la mascota con el raton
    public void MoveClicking()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                if (!EventSystem.current.IsPointerOverGameObject())
                {
                    if (hit.collider.CompareTag("Floor"))
                    {
                        agent.stoppingDistance = 0;
                        if (Vector3.Distance(hit.point, player.position) < moveRange)
                        {
                            agent.SetDestination(hit.point);
                        }
                        else
                        {
                            Vector3 dir = (hit.point - player.position).normalized;
                            agent.SetDestination(player.position + dir * moveRange);
                        }
                        agent.stoppingDistance = 0;
                    }
                    else if (hit.collider.CompareTag("Resource"))
                    {
                        agent.stoppingDistance = 0;
                        if (Vector3.Distance(hit.point, player.position) < moveRange)
                        {
                            SelectedResource(hit);
                        }
                    }
                }

            }
        }
        float distance = Vector3.Distance(agent.destination, transform.position);
        if (distance < 0.1f)
        {
            transform.LookAt(new Vector3(agent.destination.x, transform.position.y, agent.destination.z));
        }

    }
    //Cambia de forma aleatoria la posicion a la que va la mascota por el mapa
    private void RandomChangePosition()
    {
        moveing = false;
        Vector3 newPos = agent.destination + new Vector3(Random.Range(-5, 5), Random.Range(-5, 5), Random.Range(-5, 5));
        agent.SetDestination(newPos);
    }
    //Recoge los recursos
    public virtual void Loot()
    {
        anim.SetTrigger("Loot");
        float distance = Vector3.Distance(agent.destination, transform.position);
        if (distance <= stopDistance)
        {
            transform.LookAt(new Vector3(agent.destination.x, transform.position.y, agent.destination.z));
        }
    }
    //Activa y desactiva el estado de seguimiento
    public void ToggleFolowChar()
    {
        if (followingPlayer)
        {
            followingPlayer = false;
        }
        else
        {
            followingPlayer = true;
        }
    }
    //Detecta el raton encima y enseña el nombre y la vida
    private void OnMouseEnter()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            if (tag == "CharPet")
            {
                GetComponent<Outline>().enabled = true;
                PetManager.petManagerInstance.selectedPet = this;
                PetManager.petManagerInstance.ShowName(true);
                PetManager.petManagerInstance.ComparePetsUpdateMenu();
                Cursor.SetCursor(PetManager.petManagerInstance.cursorPetArrow, Vector2.zero, CursorMode.ForceSoftware);
                ControllersHelper.controllersHelperInstance.ToogleImage(true);
                ControllersHelper.controllersHelperInstance.ChangeKey(gameObject);
            }
        }
    }
    //Detecta que deseleciones la mascota y desactiva el nombre y la vida
    private void OnMouseExit()
    {
        if (tag == "CharPet")
        {
            GetComponent<Outline>().enabled = false;
            Cursor.SetCursor(PetManager.petManagerInstance.cursorArrow, Vector2.zero, CursorMode.ForceSoftware);
            if (!PetManager.petManagerInstance.petMenu.activeSelf)
            {
                PetManager.petManagerInstance.selectedPet = null;
                PetManager.petManagerInstance.ShowName(false);
            }
            if (PetManager.petManagerInstance.currentPet != null)
            {
                PetManager.petManagerInstance.selectedPet = PetManager.petManagerInstance.currentPet;
                PetManager.petManagerInstance.ShowName(true);
            }
            ControllersHelper.controllersHelperInstance.ToogleImage(false);
        }
    }

    private void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(1))
        {
            if (tag == "CharPet")
            {
                PetManager.petManagerInstance.petMenu.SetActive(true);
                ControllersHelper.controllersHelperInstance.ToogleImage(false);
            }
        }
    }

    public void CureTime()
    {
        if (modo != Mode.Combat)
        {
            life += 5;
            if (life > lifeMax)
            {
                life = lifeMax;
            }
            if (PetManager.petManagerInstance.selectedPet != null)
            {
                PetManager.petManagerInstance.UpdateUI();
            }
        }
    }
}