﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;

public class CombatController : MonoBehaviour
{
    private AnimalsController petController;
    private NavMeshAgent agent;
    public Transform enemyTarget;
    public bool attacking;
    public float attackForce = 10;
    public List<WildPetController> currentEnemy = new List<WildPetController>();
    // Start is called before the first frame update
    void Start()
    {
        petController = GetComponent<AnimalsController>();
        agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        if (petController.modo == AnimalsController.Mode.Combat)
        {
            GoToTarget();
        }
    }

    public void GoToTarget()
    {
        if (enemyTarget != null)
        {
            agent.SetDestination(enemyTarget.position);
            agent.stoppingDistance = 1.5f;
            transform.LookAt(new Vector3(enemyTarget.position.x, transform.position.y, enemyTarget.position.z));
            if (Vector3.Distance(transform.position, enemyTarget.position) < 1.5f)
            {
                if (!attacking)
                {
                    GetComponent<AnimalsController>().anim.SetBool("Attack", true);

                }
            }

            if (Input.GetMouseButtonDown(0) && petController.followingPlayer)
            {
                Ray ray = petController.cam.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    if (!EventSystem.current.IsPointerOverGameObject())
                    {
                        if (PetManager.petManagerInstance.currentPetIcon.gameObject.activeSelf)
                        {
                            if (Vector3.Distance(hit.point, petController.player.position) < petController.moveRange)
                            {
                                if (hit.collider.CompareTag("Floor"))
                                {
                                    agent.SetDestination(hit.point);
                                    petController.modo = AnimalsController.Mode.MoveControl;
                                }
                            }
                        }
                    }
                }
            }

        }
        else
        {
            StopAttacking();
        }
    }

    public void Attack()
    {
        Debug.Log("Attacked");
        if (!currentEnemy.Contains(enemyTarget.GetComponent<WildPetController>()))
        {

            currentEnemy.Add(enemyTarget.GetComponent<WildPetController>());
            enemyTarget.GetComponent<WildPetController>().petInfo.faced++;
        }
        Vector3 dir = (enemyTarget.position - transform.position).normalized;
        if (GetComponent<AnimalsController>().item.amount > 0)
        {
            int weaponDamage = GetComponent<AnimalsController>().item.objeto.ObtainEquipable().damage;
            enemyTarget.GetComponent<WildPetController>().HitDamage(petController.damage + weaponDamage, dir, gameObject);
        }
        else
        {
            enemyTarget.GetComponent<WildPetController>().HitDamage(petController.damage, dir, gameObject);
        }
        if (enemyTarget.GetComponent<WildPetController>().life <= 0)
        {
            currentEnemy.Remove(enemyTarget.GetComponent<WildPetController>());
            WildPetManager.instanceWildPetManager.ShowName(false);
            agent.SetDestination(transform.position);
            Destroy(enemyTarget.gameObject);
            StopAttacking();
        }
        attacking = true;
        GetComponent<PetExperienceController>().GainExp(45);
        Invoke(nameof(AttackCoolDown), petController.attackRate);
        GetComponent<AnimalsController>().anim.SetBool("Attack", false);
    }

    public void AttackCoolDown()
    {
        attacking = false;
    }

    public void HitDamage(int damage, Vector3 dir, GameObject enemy)
    {
        if (GetComponent<AnimalsController>().armor.amount > 0)
        {
            damage -= GetComponent<AnimalsController>().armor.objeto.ObtainEquipable().defense;
        }
        if (damage < 0)
        {
            damage = 0;
        }
        enemyTarget = enemy.transform;
        if (petController.modo == AnimalsController.Mode.Loot)
        {
            GetComponent<LooterPet>().StopLooting();
        }
        petController.modo = AnimalsController.Mode.Combat;
        petController.life -= damage;
        if (PetManager.petManagerInstance.currentPet != null && PetManager.petManagerInstance.currentPet == GetComponent<AnimalsController>())
        {
            PetManager.petManagerInstance.selectedPet = PetManager.petManagerInstance.currentPet;
            PetManager.petManagerInstance.ShowName(true);
        }
        GetComponent<Rigidbody>().isKinematic = false;
        GetComponent<Rigidbody>().AddForce(dir * attackForce);
        if (GetComponent<AnimalsController>().life > 0)
        {
            Invoke(nameof(ResetRigidbody), 0.5f);
        }
        else
        {
            PetManager.petManagerInstance.ShowName(false);
            if (GetComponent<AnimalsController>().followingPlayer)
            {

                PetManager.petManagerInstance.ToggleFollowSelectedPet();
                PetManager.petManagerInstance.followTog.isOn = false;
            }
        }
    }

    public void ResetRigidbody()
    {
        GetComponent<Rigidbody>().isKinematic = true;
    }
    private void StopAttacking()
    {
        if (petController.followingPlayer)
        {
            petController.modo = PetManager.petManagerInstance.currentPetState;
        }
        else
        {
            petController.modo = AnimalsController.Mode.StandBy;
        }


    }
}
