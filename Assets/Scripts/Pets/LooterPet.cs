﻿using UnityEngine;

public class LooterPet : AnimalsController
{
    //Detecta que la fuente de recurso es del tipo que recolecta la mascota y lo pone en modo ir a por los recursos
    public override void SelectedResource(RaycastHit hit)
    {
        if (hit.collider.CompareTag("Resource") && hit.collider.GetComponentInParent<ResourceContainer>() != resourceSource)
        {
            if (hit.collider.GetComponentInParent<ResourceContainer>().resource.resourceType == petStats.looterType)
            {
                agent.stoppingDistance = 0;
                resourceSource = hit.collider.GetComponentInParent<ResourceContainer>();
                agent.SetDestination(resourceSource.transform.position);
                modo = Mode.GoToResource;
                CancelInvoke(nameof(Loot));
                resourceSource.manager.currentLootSource = resourceSource;
            }
        }
    }
    //Recoge los materiales del recurso
    public override void Loot()
    {
        base.Loot();


    }

    public void LootTxtInstance()
    {
        if (resourceSource != null)
        {
            if (resourceSource.resource.resourceType == petStats.looterType)
            {
                int amountAdded;
                int probability = Random.Range(0, 100);
                if (probability > 75)
                {
                    amountAdded = resourceSource.resource.maxAmount;
                }
                else
                {
                    amountAdded = resourceSource.resource.minAmount;
                }
                inventory.AddItem(resourceSource.resource.resourceMaterial, amountAdded);
                if (PetManager.petManagerInstance.petInventoryPanel.inventory == inventory)
                {
                    PetManager.petManagerInstance.petInventoryPanel.OrganizeButtons();
                }
                resourceSource.life -= damage;
                //resourceSource.audioSource.pitch = Random.Range(0.6f, 1.4f);
                //resourceSource.audioSource.PlayOneShot(resourceSource.treeShake);
                resourceSource.manager.UpdateLife();

                GameObject newText = Instantiate(PetManager.petManagerInstance.actionsText, GameObject.Find("CanvasUI").gameObject.transform);
                newText.GetComponent<TextMeshPopUp>().target = gameObject;
                newText.GetComponent<TextMeshPopUp>().text.text = "+ " + amountAdded;
                newText.GetComponent<TextMeshPopUp>().img.sprite = resourceSource.resource.resourceMaterial.itemSpr;
                petStats.resourceAmount += amountAdded;
                if (resourceSource.life <= 0)
                {
                    resourceSource.removed = true;
                    resourceSource.transform.GetChild(0).gameObject.SetActive(false);
                    StopLooting();
                    resourceSource = null;
                    if (followingPlayer)
                    {
                        modo = PetManager.petManagerInstance.currentPetState;
                    }
                    else
                    {
                        modo = Mode.StandBy;
                    }
                }
            }

        }

    }
    public void StopLooting()
    {
        resourceSource.manager.currentLootSource = null;
        resourceSource.manager.ShowName(false);
        CancelInvoke(nameof(Loot));
    }
}
