﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Pet", menuName = "ScriptableObjects/PetScriptableObject", order = 7)]
public class PetAttributes : ScriptableObject
{
    [Header("Identity")]
    public string raceName;
    public Sprite iconSprite;
    public bool aggressive;
    public string petType;
    [SerializeField, Range(1,4)]public int rarity = 1;
    [SerializeField, Range(0, 100)] public int investigated;
    [Header("Stats")]
    public TextAsset statsFile;
    public int lifeMax;
    public int damageBase;
    public float attackRate;
    public int resourceDamage;
    public float lootRate;
    [Header("Data")]
    public int kills;
    public int faced;
    public int tammed;
    public int resourceAmount;
    [Header("Info")]
    public ItemInventario favoriteFood;
    public string info1, info2;
    [Header("References")]
    public GameObject petTammed;
    public Resource.ResourceType looterType;
}
