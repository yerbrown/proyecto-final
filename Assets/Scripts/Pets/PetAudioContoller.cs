﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetAudioContoller : CharacterAudioController
{
    public AudioClip lootSound, punchSound;

    public void PLayLoot()
    {
        audioS.pitch = Random.Range(0.6f, 1.4f);
        audioS.PlayOneShot(lootSound);
        transform.root.GetComponent<LooterPet>().LootTxtInstance();
    }

    public void PlayAttack()
    {
        audioS.pitch = Random.Range(0.6f, 1.4f);
        audioS.PlayOneShot(punchSound);
        if (transform.root.TryGetComponent<CombatController>(out CombatController combat))
        {
            combat.Attack();
        }
        else if (transform.root.TryGetComponent<WildPetController>(out WildPetController wildCombat))
        {
            wildCombat.Attack();
        }
    }
}
