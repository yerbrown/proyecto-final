﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetExperienceController : MonoBehaviour
{
    public int currentExperience, experienceToLvlUp;
    public int level;
    private void Awake()
    {
        PetLvlManager.petLvlManagerInstance.ChangeExpToLvlUp(GetComponent<AnimalsController>().petStats.statsFile, gameObject);
    }
    public void GainExp(int exp)
    {
        currentExperience += exp;
        if (currentExperience>=experienceToLvlUp)
        {
            int rest = currentExperience - experienceToLvlUp;
            currentExperience = rest;
            PetLvlManager.petLvlManagerInstance.ChangeExpToLvlUp(GetComponent<AnimalsController>().petStats.statsFile, gameObject);
        }
    }
}
