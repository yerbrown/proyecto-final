﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class PetInstantiateManager : MonoBehaviour
{
    public static PetInstantiateManager instantiateManagerInstance;
    public PetTammed petList;
    public List<AnimalsController> inScenePets = new List<AnimalsController>();
    public List<GameObject> petPrefabs = new List<GameObject>();

    public Animation saveAnim;
    private void Awake()
    {
        instantiateManagerInstance = this;
    }
    private void Start()
    {
        InstantiatePetsInScene();
        InstantiateCharPetFollow();
        loadScenes.loadScenesInstance.saveData.AddListener(SavePetData);
    }

    public void InstantiateCharPetFollow()
    {
        if (CharacterMovement.characterMovementInstance.charSaveData.foollowingPetKey != "")
        {
            PetManager.petManagerInstance.selectedPet = SearchPetGameObjectByKey(CharacterMovement.characterMovementInstance.charSaveData.foollowingPetKey);
            PetManager.petManagerInstance.currentPet = SearchPetGameObjectByKey(CharacterMovement.characterMovementInstance.charSaveData.foollowingPetKey);
            PetManager.petManagerInstance.followTog.isOn = true;
            PetManager.petManagerInstance.ShowName(true);
        }
    }
    public void InstantiatePetsInScene()
    {
        foreach (PetTammed.PetAll pet in petList.allPets)
        {
            if (pet.currentScene == SceneManager.GetActiveScene().buildIndex&& pet.inStable==false)
            {
                InstatiatePet(pet);
            }
        }
    }


    public void InstatiatePet(PetTammed.PetAll pet)
    {
        for (int i = 0; i < petPrefabs.Count; i++)
        {
            if (pet.petRaceInfo.raceName == petPrefabs[i].GetComponent<AnimalsController>().petStats.raceName)
            {
                GameObject newPet = Instantiate(petPrefabs[i], ReplaceInWorldPos(pet), ReplaceInWorldRot(pet));
                inScenePets.Add(newPet.GetComponent<AnimalsController>());
                ResetPetData(newPet.GetComponent<AnimalsController>(), pet);
                break;
            }
        }
    }
    public void ResetPetData(AnimalsController nextAnimal, PetTammed.PetAll pet)
    {
        nextAnimal.petStats = pet.petRaceInfo;
        nextAnimal.petName = pet.petName;
        nextAnimal.life = pet.life;
        nextAnimal.lifeMax = pet.lifeMax;
        nextAnimal.attackRate = pet.attackRate;
        nextAnimal.lootRate = pet.lootRate;
        nextAnimal.damage = pet.damage;
        nextAnimal.inventory = pet.inventory;
        nextAnimal.armor = pet.armor;
        nextAnimal.item = pet.item;
        nextAnimal.food = pet.food;
        nextAnimal.animalKey = pet.petKey;

        nextAnimal.GetComponent<PetExperienceController>().currentExperience = pet.currentExperience;
        nextAnimal.GetComponent<PetExperienceController>().experienceToLvlUp = pet.experienceToLvlUp;
        nextAnimal.GetComponent<PetExperienceController>().level = pet.level;
    }

    public Vector3 ReplaceInWorldPos(PetTammed.PetAll pet)
    {
        return pet.pos;

    }

    public Quaternion ReplaceInWorldRot(PetTammed.PetAll pet)
    {
        return pet.rot;

    }
    public void SavePetData()
    {
        saveAnim.Play();
        foreach (AnimalsController pet in inScenePets)
        {
            for (int i = 0; i < petList.allPets.Count; i++)
            {
                if (petList.allPets[i].petKey == pet.animalKey)
                {
                    petList.allPets[i].petName = pet.petName;
                    petList.allPets[i].life = pet.life;
                    petList.allPets[i].lifeMax = pet.lifeMax;
                    petList.allPets[i].attackRate = pet.attackRate;
                    petList.allPets[i].lootRate = pet.lootRate;
                    petList.allPets[i].damage = pet.damage;
                    petList.allPets[i].inventory = pet.inventory;
                    petList.allPets[i].armor = pet.armor;
                    petList.allPets[i].item = pet.item;
                    petList.allPets[i].food = pet.food;

                    petList.allPets[i].currentScene = SceneManager.GetActiveScene().buildIndex;
                    petList.allPets[i].pos = pet.transform.position;
                    petList.allPets[i].rot = pet.transform.rotation;

                    petList.allPets[i].currentExperience = pet.GetComponent<PetExperienceController>().currentExperience;
                    petList.allPets[i].experienceToLvlUp = pet.GetComponent<PetExperienceController>().experienceToLvlUp;
                    petList.allPets[i].level = pet.GetComponent<PetExperienceController>().level;
                    break;
                }
            }
        }
    }

    public PetTammed.PetAll SearchPetByKey(string petKey)
    {
        for (int i = 0; i < petList.allPets.Count; i++)
        {
            if (petList.allPets[i].petKey == petKey)
            {
                return petList.allPets[i];
            }
        }
        return null;
    }

    public AnimalsController SearchPetGameObjectByKey(string petKey)
    {
        for (int i = 0; i < inScenePets.Count; i++)
        {
            if (inScenePets[i].animalKey == petKey)
            {
                return inScenePets[i];
            }
        }
        return null;
    }
}
