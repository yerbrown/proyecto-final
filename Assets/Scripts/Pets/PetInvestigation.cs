﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PetInvestigation : MonoBehaviour
{
    public PetAttributes currentPet;
    public Text petname;
    public Image petIcon;
    public Image currentInvestigation;
    public Text investigationPercent;

    public void UpdateUI()
    {
        petname.text = currentPet.raceName;
        petIcon.sprite = currentPet.iconSprite;
        currentInvestigation.fillAmount = (float)currentPet.investigated / 100;
        if (currentPet.investigated>100)
        {
            currentPet.investigated = 100;
        }
        investigationPercent.text = "%" + currentPet.investigated;

    }

    public void UpdatePetInfo()
    {
        WikiPetManager.wikiPetManagerInstance.UpdatePetInfo(currentPet);
    }

    public void PlaySound()
    {
        AudioManager.audioSourceInstance.PlayGeneralClickSound();
    }
}
