﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetLvlManager : MonoBehaviour
{
    public static PetLvlManager petLvlManagerInstance;
    [System.Serializable]
    public class PetLvlStats
    {
        public string lvl;
        public string[] values;
        public Dictionary<string, string> valuePerStat = new Dictionary<string, string>();

        public PetLvlStats(string fila)
        {
            values = fila.Split(";"[0]);
            lvl = values[0];
        }
    }

    public TextAsset textFile;
    public PetLvlStats[] levelStats;
    public string[] filas;
    private void Awake()
    {
        petLvlManagerInstance = this;
    }
    public void ReadFile()
    {
        filas = textFile.text.Split("\n"[0]);
        levelStats = new PetLvlStats[filas.Length];
        for (int i = 0; i < filas.Length; i++)
        {
            levelStats[i] = new PetLvlStats(filas[i]);
            for (int j = 0; j < levelStats[0].values.Length; j++)
            {
                string stat = levelStats[0].values[j];
                if (stat.Contains("\r"))
                {
                    stat = stat.Replace("\r","");
                }
                levelStats[i].valuePerStat.Add(stat, levelStats[i].values[j]);
            }
        }
    }
    public void ChangeExpToLvlUp(TextAsset petStats, GameObject pet)
    {
        textFile = petStats;
        ReadFile();
        PetExperienceController petExp = pet.GetComponent<PetExperienceController>();
        petExp.level += 1;
        petExp.experienceToLvlUp = int.Parse(levelStats[petExp.level].valuePerStat["exp"]);
        AnimalsController petAtrb = pet.GetComponent<AnimalsController>();
        petAtrb.damage = int.Parse(levelStats[petExp.level].valuePerStat["attack"]);
        petAtrb.lifeMax = int.Parse(levelStats[petExp.level].valuePerStat["lifemax"]);
    }
}
