﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PetManager : MonoBehaviour
{
    public static PetManager petManagerInstance;
    [Header("Pets")]
    public AnimalsController currentPet, selectedPet;
    public List<GameObject> petPrefabs;
    [Header("UI")]
    public Text nameUI;
    public Image lifeUI;
    public Image nameIcon;
    public Toggle followTog;
    public InputField inputFName;
    public Button petInventoryButton;
    public GameObject petMenu;
    public Button petControl;
    public Image currentPetIcon;
    public Text currentPetName;
    public GameObject actionsText;
    public Texture2D cursorArrow, cursorPetArrow;
    [Header("Acces")]
    public GameObject characater;
    public CameraControll cam;
    public InventoryButtonManager petInventoryPanel;
    public AnimalsController.Mode currentPetState;
    public StatsViewer petStatsMenu;
    private void Awake()
    {
        petManagerInstance = this;
    }
    // Update is called once per frame
    void Update()
    {
        if (selectedPet != null)
        {
            ClampName();
        }
        if (currentPetState == AnimalsController.Mode.MoveControl)
        {
            CharacterInventory.characterInventoryInstance.GetComponent<CharacterMovement>().canMove = false;
        }
    }

    //Compara si ya tienes una mascota siguiendote y activa o desactiva el menu de mascota
    public void ComparePetsUpdateMenu()
    {
        if (currentPet != null)
        {
            if (currentPet == selectedPet)
            {
                ActivatePetmenuInteractive(true);
            }
            else
            {
                ActivatePetmenuInteractive(false);
            }

        }
        else
        {
            ActivatePetmenuInteractive(true);
        }
    }
    //Activa o desactiva el menu interactivo
    public void ActivatePetmenuInteractive(bool activate)
    {
        followTog.interactable = activate;
        inputFName.interactable = activate;
    }
    //Asigna el inventario de la mascota que se quiere visualizar
    public void AssignInventoryToPanel()
    {
        petInventoryPanel.inventory = selectedPet.inventory;
    }

    //Asigna las estadisticas de la mascota a la tarjeta de mascota
    public void AssignStatsPet()
    {
        petStatsMenu.UpdateUI(selectedPet.gameObject);
    }
    //Activa o desactiva el seguimineto de la mascota hacia el player
    public void ToggleFollowSelectedPet()
    {
        selectedPet.ToggleFolowChar();
        if (selectedPet.followingPlayer)
        {
            currentPet = selectedPet;
            CharacterMovement.characterMovementInstance.charSaveData.foollowingPetKey = PetInstantiateManager.instantiateManagerInstance.SearchPetByKey(currentPet.animalKey).petKey;
            currentPet.player = characater.transform;
            petControl.gameObject.SetActive(true);
            petControl.GetComponent<Image>().sprite = currentPet.petStats.iconSprite;
            currentPetState = AnimalsController.Mode.Follow;
            if (currentPet.modo == AnimalsController.Mode.StandBy)
            {
                currentPet.modo = currentPetState;
            }

        }
        else
        {
            if (currentPetIcon.gameObject.activeSelf)
            {
                ActivatePetControlMode();
            }
            if (currentPet.modo == AnimalsController.Mode.Follow)
            {
                currentPet.modo = AnimalsController.Mode.StandBy;
            }
            currentPet = null;
            CharacterMovement.characterMovementInstance.charSaveData.foollowingPetKey = "";
            petControl.gameObject.SetActive(false);
        }
    }
    //Cambia el nombre de la mascota seleccionada
    public void ChangeName()
    {
        selectedPet.petName = inputFName.text;
        nameUI.text = selectedPet.petName;
    }
    //Enseña el nombre y la vida de la mascota
    public void ShowName(bool active)
    {
        if (active)
        {
            UpdateUI();
        }
        nameUI.gameObject.SetActive(active);
    }
    //Actualiza la interfaz de la mascota
    public void UpdateUI()
    {
        nameUI.text = selectedPet.petName;
        lifeUI.fillAmount = (float)selectedPet.life / selectedPet.lifeMax;
        nameIcon.sprite = selectedPet.petStats.iconSprite;

    }
    //Fija el nombre de la mascota seleccionada en la interfaz
    public void ClampName()
    {
        Vector3 namePos = Camera.main.WorldToScreenPoint(selectedPet.nameTrans.transform.position);
        nameUI.transform.position = namePos;
    }
    //Activa y desactiva el control de la mascota
    public void ActivatePetControlMode()
    {
        if (currentPet != null)
        {
            if (currentPetIcon.gameObject.activeSelf)
            {
                characater.GetComponent<CharacterMovement>().canMove = true;
                currentPetIcon.gameObject.SetActive(false);
                currentPetState = AnimalsController.Mode.Follow;
                cam.target = characater.transform;
            }
            else
            {
                characater.GetComponent<CharacterMovement>().canMove = false;
                currentPetIcon.sprite = currentPet.petStats.iconSprite;
                currentPetIcon.gameObject.SetActive(true);
                currentPetName.text = currentPet.petName;
                currentPetState = AnimalsController.Mode.MoveControl;
                cam.target = currentPet.transform;
            }
            SwitchState();
        }

    }
    //Cambia el estado de la mascota
    public void SwitchState()
    {
        if (currentPet.modo == AnimalsController.Mode.MoveControl || currentPet.modo == AnimalsController.Mode.Follow)
        {
            currentPet.modo = currentPetState;
        }
    }



}
