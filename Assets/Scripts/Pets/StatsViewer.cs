﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatsViewer : MonoBehaviour
{
    public Text razeName;
    public Text life;
    public Image lifeImg;
    public Text exp;
    public Image expImg;
    public Text lvlText;
    public Text attack;
    public Text lootDmg;
    public Image petImg;
    public Image armorImg;
    public Text armorTxt;
    public Image itemImg;
    public Text itemTxt;
    public Image foodImg;
    public Text lifeTxt, hambreTxt, sedTxt;
    public GameObject currentPet;
    public void UpdateUI(GameObject pet)
    {
        //if (currentPet!=pet)
        //{
        //    currentPet = pet;
        //}
        AnimalsController petStats = pet.GetComponent<AnimalsController>();
        PetExperienceController petExp = pet.GetComponent<PetExperienceController>();
        razeName.text = petStats.petStats.raceName;
        life.text = petStats.life + "/" + petStats.lifeMax;
        lifeImg.fillAmount = (float)petStats.life / petStats.lifeMax;
        exp.text = petExp.currentExperience + "/" + petExp.experienceToLvlUp;
        expImg.fillAmount = (float)petExp.currentExperience / petExp.experienceToLvlUp;
        lvlText.text = "Lv " + petExp.level;
        attack.text = petStats.damage.ToString();
        lootDmg.text = petStats.petStats.resourceDamage.ToString();
        petImg.sprite = petStats.petStats.iconSprite;
        if (petStats.armor.amount > 0)
        {
            armorImg.enabled = true;
            armorImg.sprite = petStats.armor.objeto.itemSpr;
            armorTxt.text = petStats.armor.objeto.ObtainEquipable().defense.ToString("00");
        }
        else
        {
            armorImg.enabled = false;
            armorTxt.text = 00.ToString("00");
        }
        if (petStats.item.amount > 0)
        {
            itemImg.enabled = true;
            itemImg.sprite = petStats.item.objeto.itemSpr;
            itemTxt.text = petStats.item.objeto.ObtainEquipable().damage.ToString("00");
        }
        else
        {
            itemImg.enabled = false;
            itemTxt.text = 00.ToString("00");
        }
        if (petStats.food.amount > 0)
        {
            foodImg.enabled = true;
            foodImg.sprite = petStats.food.objeto.itemSpr;
            lifeTxt.text = petStats.food.objeto.ObtainConsumible().vidaSum.ToString("00");
            sedTxt.text = petStats.food.objeto.ObtainConsumible().sedSum.ToString("00");
            hambreTxt.text = petStats.food.objeto.ObtainConsumible().hambreSum.ToString("00");
        }
        else
        {
            foodImg.enabled = false;
            lifeTxt.text = 0.ToString("00");
            sedTxt.text = 0.ToString("00");
            hambreTxt.text = 0.ToString("00");
        }
        
        

    }

    public void ToogleTimeScale()
    {
        if (gameObject.activeSelf)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }
    }
}
