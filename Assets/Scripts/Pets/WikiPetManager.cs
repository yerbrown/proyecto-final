﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WikiPetManager : MonoBehaviour
{
    public static WikiPetManager wikiPetManagerInstance;
    [Header("Pets")]
    public List<PetAttributes> petsFaced = new List<PetAttributes>();
    public GameObject petsPanel;
    public GameObject petCaseInvestPrefab;
    [Header("PetUI")]
    public Text petName;
    public Image petImg;
    public Text aggressiveText;
    public Text typeText;
    public Image rarity;
    public Text killsText;
    public Text facedText;
    public Text tammedText;
    public Text resourceAmountText;
    public Image foodImage;
    public Text info1, info2;

    public void Awake()
    {
        wikiPetManagerInstance = this;
    }

    private void OnEnable()
    {
        DeleteButtons();
        UpdatePets();
    }

    public void DeleteButtons()
    {
        for (int i = petsPanel.transform.childCount - 1; i >= 0; i--)
        {
            Destroy(petsPanel.transform.GetChild(i).gameObject);
        }
    }
    public void UpdatePets()
    {
        for (int i = 0; i < petsFaced.Count; i++)
        {
            GameObject newPetCase = Instantiate(petCaseInvestPrefab, petsPanel.transform);
            newPetCase.GetComponent<PetInvestigation>().currentPet = petsFaced[i];
            newPetCase.GetComponent<PetInvestigation>().UpdateUI();
        }
    }
    public void UpdatePetInfo(PetAttributes pet)
    {
        petName.text = pet.raceName;
        petImg.sprite = pet.iconSprite;
        aggressiveText.text = pet.aggressive.ToString();
        typeText.text = pet.petType;
        rarity.fillAmount = (float)pet.rarity / 4;
        killsText.text = pet.kills.ToString("000");
        facedText.text = pet.faced.ToString("000");
        tammedText.text = pet.tammed.ToString("000");
        resourceAmountText.text = pet.resourceAmount.ToString("00000");
        if (pet.investigated >= 20)
        {
            info1.text = pet.info1;
            foodImage.sprite = pet.favoriteFood.itemSpr;
        }
        else
        {
            info1.text = "??";
            foodImage.sprite = null;
        }
        if (pet.investigated >= 80)
        {
            info2.text = pet.info2;

        }
        else
        {
            info2.text = "??";
        }
    }

    public void Pause()
    {
        if (wikiPetManagerInstance.gameObject.activeSelf)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }
    }

    public void ResetWiki()
    {
        petName.text = "Pet Name";
        petImg.sprite = null;
        aggressiveText.text = "??";
        typeText.text = "??";
        rarity.fillAmount = 0;
        killsText.text = "000";
        facedText.text = "000";
        tammedText.text = "000";
        resourceAmountText.text = "00000";
        foodImage.sprite = null;
        info1.text = "??";
        info2.text = "??";
    }
}
