﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;
using UnityEngine.EventSystems;

public class WildPetController : MonoBehaviour
{
    public Transform nameTrans;
    public PetAttributes petInfo;
    public Animator anim;
    public bool moveing;
    public NavMeshAgent agent;
    private WildPetManager wildPetManager;
    public PetStates petEstado;
    public GameObject target;
    public float visionRange;
    public float forceAmount = 100;
    public bool attacking;
    [Header("Atributes")]
    public int life;
    public enum PetStates
    {
        Deambulando,
        Siguiendo,
        MirarAlrededor,
        Combatiendo
    }


    // Establece el manager de las mascotas salvajes
    void Start()
    {
        wildPetManager = WildPetManager.instanceWildPetManager;
        life = petInfo.lifeMax;
    }

    // Update is called once per frame
    void Update()
    {
        switch (petEstado)
        {
            case PetStates.Deambulando:
                if (moveing == false)
                {
                    moveing = true;
                    Invoke(nameof(RandomChangePosition), 5);
                }

                break;
            case PetStates.Siguiendo:
                FollowTarget();
                break;
            case PetStates.MirarAlrededor:

                break;
            case PetStates.Combatiendo:
                CombatTarget();
                break;
            default:
                break;
        }
        anim.SetFloat("Forward", agent.velocity.magnitude / agent.speed);
    }

    public void HitDamage(int damage, Vector3 dir, GameObject enemyTarget)
    {
        petInfo.investigated += 2;
        target = enemyTarget;
        petEstado = PetStates.Siguiendo;
        life -= damage;
        ShowLife();
        wildPetManager.UpdateUI();
        GetComponent<Rigidbody>().isKinematic = false;
        GetComponent<Rigidbody>().AddForce(dir * forceAmount);
        if (life > 0)
        {
            Invoke(nameof(ResetRigidbody), 0.5f);
        }
        else
        {
            petInfo.investigated += 15;
            petInfo.kills++;
            wildPetManager.ShowName(false);
            CancelInvoke(nameof(ResetRigidbody));
        }
    }
    //Cambia de forma aleatoria la posicion a la que se mueve la mascota
    private void RandomChangePosition()
    {
        moveing = false;
        Vector3 newPos = agent.destination + new Vector3(Random.Range(-5, 5), Random.Range(-5, 5), Random.Range(-5, 5));
        agent.SetDestination(newPos);
    }

    //Detecta si ve al player o a la mascota del player y cambia de estado
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("CharPet") && target == null)
        {
            if (petInfo.aggressive)
            {
                target = other.gameObject;
                petEstado = PetStates.Siguiendo;
                CancelInvoke(nameof(RandomChangePosition));
            }
        }
    }
    //Detecta si colocas el raton encima y te enseña el nombre y su vida
    private void OnMouseEnter()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            GetComponent<Outline>().enabled = true;
            wildPetManager.selectedWildPet = this;
            wildPetManager.ShowName(true);
            if (PetManager.petManagerInstance.currentPet != null && PetManager.petManagerInstance.currentPetState == AnimalsController.Mode.MoveControl)
            {
                Cursor.SetCursor(wildPetManager.cursorAttackArrow, Vector2.zero, CursorMode.ForceSoftware);
                ControllersHelper.controllersHelperInstance.ToogleImage(true);
                ControllersHelper.controllersHelperInstance.ChangeKey(gameObject);
            }
            else
            {
                Cursor.SetCursor(wildPetManager.cursorWildPetArrow, Vector2.zero, CursorMode.ForceSoftware);
            }

        }
    }
    //Detecta si deselecionas a la mascota salvaje
    private void OnMouseExit()
    {
        if (wildPetManager.currentWildPet != null)
        {
            wildPetManager.selectedWildPet = wildPetManager.currentWildPet;
            wildPetManager.ShowName(true);
        }
        else
        {
            wildPetManager.ShowName(false);
        }
        GetComponent<Outline>().enabled = false;
        ControllersHelper.controllersHelperInstance.ToogleImage(false);
        Cursor.SetCursor(wildPetManager.cursorArrow, Vector2.zero, CursorMode.ForceSoftware);
    }
    //Sigue al objetivo y si se separa demasiado vuelve a deambular por el mapa
    public void FollowTarget()
    {
        if (Vector3.Distance(transform.position, target.transform.position) <= agent.stoppingDistance + 0.5f)
        {
            petEstado = PetStates.Combatiendo;
            transform.LookAt(new Vector3(target.transform.position.x, transform.position.y, target.transform.position.z));
        }
        else if (Vector3.Distance(transform.position, target.transform.position) < visionRange)
        {
            agent.SetDestination(target.transform.position);
            transform.LookAt(new Vector3(target.transform.position.x, transform.position.y, target.transform.position.z));
        }
        else
        {
            target = null;
            petEstado = PetStates.Deambulando;
        }
    }
    //Sistema de combate
    public void CombatTarget()
    {
        if (target != null)
        {
            if (Vector3.Distance(transform.position, target.transform.position) < 1.5f)
            {
                if (!attacking)
                {
                    anim.SetBool("Attack", true);
                    
                }
            }
            else
            {
                petEstado = PetStates.Siguiendo;
            }
            transform.LookAt(new Vector3(target.transform.position.x, transform.position.y, target.transform.position.z));
        }
        else
        {
            petEstado = PetStates.Deambulando;
        }
    }
    public void ResetRigidbody()
    {
        GetComponent<Rigidbody>().isKinematic = true;
    }

    public void Attack()
    {
        Debug.Log("Attacked");
        attacking = true;
        Vector3 dir = (target.transform.position - transform.position).normalized;
        target.GetComponent<CombatController>().HitDamage(petInfo.damageBase, dir, gameObject);
        ShowLife();
        if (target.GetComponent<AnimalsController>().life <= 0)
        {
            wildPetManager.currentWildPet = null;
            wildPetManager.selectedWildPet = null;
            wildPetManager.ShowName(false);
            agent.SetDestination(transform.position);
            petEstado = PetStates.Deambulando;
            Destroy(target.gameObject);
        }
        Invoke(nameof(AttackCoolDown), petInfo.attackRate);
        anim.SetBool("Attack", false);
    }
    public void AttackCoolDown()
    {
        attacking = false;
    }

    public void ShowLife()
    {
        if (wildPetManager.currentWildPet == null)
        {
            wildPetManager.currentWildPet = this;
            wildPetManager.selectedWildPet = wildPetManager.currentWildPet;
            wildPetManager.ShowName(true);
        }
    }
}
