﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WildPetManager : MonoBehaviour
{
    public static WildPetManager instanceWildPetManager;
    public WildPetController currentWildPet, selectedWildPet;
    [Header("UI")]
    public Text nameUI;
    public Image lifeUI, tammingUI;
    public PetManager petManager;
    public Texture2D cursorArrow, cursorWildPetArrow, cursorAttackArrow;


    private void Awake()
    {
        instanceWildPetManager = this;
    }

    private void Update()
    {
        if (selectedWildPet != null)
        {
            ClampName();
        }
    }

    //FIja el nombre de la mascota en la interfaz siguiendola por la pantalla
    public void ClampName()
    {
        if (selectedWildPet != null)
        {
            Vector3 namePos = Camera.main.WorldToScreenPoint(selectedWildPet.nameTrans.transform.position);
            nameUI.transform.position = namePos;
        }
    }
    //Enseña el nombre de la mascota 
    public void ShowName(bool active)
    {
        if (active)
        {
            UpdateUI();
        }
        nameUI.gameObject.SetActive(active);
    }

    public void UpdateUI()
    {
        nameUI.text = selectedWildPet.petInfo.raceName;
        lifeUI.fillAmount = (float)selectedWildPet.life / selectedWildPet.petInfo.lifeMax;
        if (selectedWildPet.GetComponent<WildPetTamming>().currentTam > 0)
        {
            tammingUI.transform.parent.gameObject.SetActive(true);
            tammingUI.fillAmount = selectedWildPet.GetComponent<WildPetTamming>().currentTam / selectedWildPet.GetComponent<WildPetTamming>().maxTam;
        }
        else
        {
            tammingUI.transform.parent.gameObject.SetActive(false);
        }
        if (selectedWildPet.GetComponent<WildPetTamming>().hungry)
        {
            tammingUI.color = new Color(tammingUI.color.r, tammingUI.color.g, tammingUI.color.b, 1);
        }
        else
        {
            tammingUI.color = new Color(tammingUI.color.r, tammingUI.color.g, tammingUI.color.b, 0.2f);
        }
    }
}
