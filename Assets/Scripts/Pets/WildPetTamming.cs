﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class WildPetTamming : MonoBehaviour
{
    public float hungryTime;
    public float currentTam, maxTam;
    public bool hungry = true;
    public GameObject petTammed;

    private void Start()
    {
        petTammed = GetComponent<WildPetController>().petInfo.petTammed;
    }
    public void Feed(ItemButton food)
    {
        if (hungry)
        {
            food.item.amount--;
            if (food.item.amount == 0)
            {
                CharacterInventory.characterInventoryInstance.inventory.itemsInInventory.Remove(food.item);
            }
            currentTam += 20;
            hungry = false;
            Invoke(nameof(RestartHungry), hungryTime);
            WildPetManager.instanceWildPetManager.UpdateUI();
            if (currentTam >= maxTam)
            {
                petTammed.GetComponent<AnimalsController>().petStats.tammed++;
                GameObject newPet = Instantiate(petTammed, transform.position, transform.rotation);
                newPet.GetComponent<AnimalsController>().inventory = CreateInventory();
                new PetTammed.PetAll(newPet.GetComponent<AnimalsController>(), ref PetInstantiateManager.instantiateManagerInstance.petList.allPets, ref PetInstantiateManager.instantiateManagerInstance.petList.numPets);
                PetInstantiateManager.instantiateManagerInstance.inScenePets.Add(newPet.GetComponent<AnimalsController>());
                newPet.GetComponent<AnimalsController>().animalKey = PetInstantiateManager.instantiateManagerInstance.petList.allPets[PetInstantiateManager.instantiateManagerInstance.petList.allPets.Count - 1].petKey;
                PetInstantiateManager.instantiateManagerInstance.SavePetData();
                Destroy(gameObject);
                WildPetManager.instanceWildPetManager.ShowName(false);
            }
        }
    }

    private void RestartHungry()
    {
        hungry = true;
        WildPetManager.instanceWildPetManager.UpdateUI();
    }

    private Inventario CreateInventory()
    {
        DirectoryInfo info = new DirectoryInfo("Assets/Scripts/Items/Inventarios/Pet");
        Inventario newInventario = ScriptableObject.CreateInstance<Inventario>();
        AssetDatabase.CreateAsset(newInventario, "Assets/Scripts/Items/Inventarios/Pet/Pet" + (PetInstantiateManager.instantiateManagerInstance.petList.numPets) + ".asset");
        AssetDatabase.SaveAssets();
        return newInventario;
    }
}
