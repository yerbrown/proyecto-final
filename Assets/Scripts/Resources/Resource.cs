﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Resource", menuName = "ScriptableObjects/ResourceScriptableObject", order = 6)]
public class Resource : ScriptableObject
{
    public string resourceName;
    public ItemInventario resourceMaterial;
    public int lifeMax = 100;
    public int minAmount, maxAmount;
    public Texture2D cursorResourceArrow;
    public ResourceType resourceType;
    public enum ResourceType
    {
        Wood,
        Stone
    }

}
