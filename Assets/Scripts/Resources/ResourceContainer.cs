﻿using UnityEngine;
using UnityEngine.EventSystems;

public class ResourceContainer : MonoBehaviour
{
    public Resource resource;
    public int life, lifeMax;
    public Transform nameTrans;
    public ResourcesManager manager;
    public AudioSource audioSource;
    public AudioClip treeShake;

    public float respawnTime;
    public float currentTime;
    public bool removed;
    private void Start()
    {
        if (lifeMax == 0)
        {
            lifeMax = resource.lifeMax;
        }
        life = lifeMax;
        manager = ResourcesManager.resourceManagerInstance;
    }

    private void Update()
    {
        if (removed)
        {
            currentTime += Time.deltaTime;
            if (currentTime >= respawnTime)
            {
                currentTime = 0;
                life = lifeMax;
                removed = false;
                transform.GetChild(0).gameObject.SetActive(true);
            }
        }
    }
    private void OnMouseEnter()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            GetComponent<Outline>().enabled = true;
            manager.selectedResource = this;
            manager.nameUI.GetComponent<LenguageChanger>().keyWord = resource.resourceName;
            manager.ShowName(true);

            if (PetManager.petManagerInstance.currentPet != null && PetManager.petManagerInstance.currentPetState == AnimalsController.Mode.MoveControl
                && PetManager.petManagerInstance.currentPet.petStats.looterType == resource.resourceType)
            {
                Cursor.SetCursor(resource.cursorResourceArrow, Vector2.zero, CursorMode.ForceSoftware);
                ControllersHelper.controllersHelperInstance.ToogleImage(true);
                ControllersHelper.controllersHelperInstance.ChangeKey(gameObject);
            }
            else
            {
                Cursor.SetCursor(manager.cursorResourceArrow, Vector2.zero, CursorMode.ForceSoftware);
            }
        }
    }

    private void OnMouseExit()
    {
        GetComponent<Outline>().enabled = false;
        Cursor.SetCursor(CharacterAttributeManager.characterAttributeManagerInstance.cursorArrow, Vector2.zero, CursorMode.ForceSoftware);
        if (manager.currentLootSource == null)
        {
            manager.ShowName(false);
            manager.selectedResource = null;
        }
        else
        {
            manager.selectedResource = manager.currentLootSource;
            manager.ShowName(true);
        }
        ControllersHelper.controllersHelperInstance.ToogleImage(false);
    }

}
