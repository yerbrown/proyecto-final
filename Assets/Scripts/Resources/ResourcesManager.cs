﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResourcesManager : MonoBehaviour
{
    public static ResourcesManager resourceManagerInstance;
    public ResourceContainer selectedResource, currentLootSource;
    public List<GameObject> allResourcesActive = new List<GameObject>();
    [Header("UI")]
    public Text nameUI, resourceLifeAmount;
    public Image lifeUI;
    public Texture2D cursorResourceArrow;

    private void Awake()
    {
        resourceManagerInstance = this;
    }
    void Update()
    {
        if (selectedResource != null)
        {
            ClampName();
        }
    }
    private void OnEnable()
    {
        ResourceContainer[] allResources;
        allResources = FindObjectsOfType<ResourceContainer>();
        foreach (var resourceContainer in allResources)
        {
            allResourcesActive.Add(resourceContainer.gameObject);
        }
    }
    public void ShowName(bool active)
    {
        if (active)
        {
            UpdateLife();
        }
        nameUI.gameObject.SetActive(active);
    }
    public void UpdateLife()
    {
        nameUI.text = selectedResource.resource.resourceName;
        resourceLifeAmount.text = selectedResource.life + " / " + selectedResource.lifeMax;
        lifeUI.fillAmount = (float)selectedResource.life / selectedResource.lifeMax;
    }
    public void ClampName()
    {
        Vector3 namePos = Camera.main.WorldToScreenPoint(selectedResource.nameTrans.position);
        nameUI.transform.position = namePos;
    }
}
