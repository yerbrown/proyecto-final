﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TextMeshPopUp : MonoBehaviour
{
    public GameObject target;
    public TextMeshProUGUI text;
    public Image img;
    public float offset;
    private void Start()
    {
        Destroy(gameObject, 1);
    }
    private void Update()
    {
        ClampText();
    }

    public void ClampText()
    {
        if (target != null)
        {
            Vector3 textPos = Camera.main.WorldToScreenPoint(target.GetComponent<AnimalsController>().nameTrans.position+Vector3.up*offset);
            transform.position = textPos;
        }
    }
}
