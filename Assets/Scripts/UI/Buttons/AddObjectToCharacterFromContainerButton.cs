﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddObjectToCharacterFromContainerButton : ItemButton
{
    //Añade el item del container a el invenatrio del Player
    public void AddToCharacterBag()
    {
        CharacterInventory.characterInventoryInstance.inventory.AddItem(item.objeto, item.amount);
        inventory.itemsInInventory.Remove(item);
        inventoryManager.SortInventoryItem();
    }
}
