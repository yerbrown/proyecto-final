﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipItemButton : ItemButton
{
    //Pasa el item al inventario del Player
    public void AddToCharacterBag()
    {
        CharacterInventory.characterInventoryInstance.inventory.AddItem(item.objeto, item.amount);
    }
}
