﻿using UnityEngine;
using UnityEngine.UI;

public class ItemButton : MonoBehaviour
{
    public Inventario.Item item;
    public Inventario inventory;
    public InventoryButtonManager inventoryManager;
    public AudioClip clickSound;
    [Header("UI")]
    public Image buttonImg;
    public Text nameText, amountText;
    //Actualiza el boton del item
    public virtual void UpdateButtonUI()
    {
        //nameText.GetComponent<LenguageChanger>().Traduct();
        buttonImg.sprite = item.objeto.itemSpr;
        amountText.text = item.amount.ToString();
    }
    public void PlaySoundClick()
    {
        AudioManager.audioSourceInstance.audioSource.PlayOneShot(clickSound);
    }
    public void ShowInfo()
    {
        ItemInfoViewer.itemInfoViewerInstance.gameObject.SetActive(true);
        ItemInfoViewer.itemInfoViewerInstance.UpdateUI(item, transform.position);
    }
    public void OpenInfo()
    {
        Invoke(nameof(ShowInfo), 1);
    }
    public void CloseInfo()
    {
        CancelInvoke(nameof(ShowInfo));
        if (ItemInfoViewer.itemInfoViewerInstance.gameObject.activeSelf)
        {
            ItemInfoViewer.itemInfoViewerInstance.gameObject.SetActive(false);
        }

    }
}
