﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ItemDragHandler : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler
{
    CanvasGroup canvasGroup;
    public InventoryButtonManager manager;
    public Transform lastParent;
    public AudioClip endDragSound;
    private void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }
    public void OnBeginDrag(PointerEventData eventData)
    {
        lastParent = transform.parent;
        canvasGroup.blocksRaycasts = false;
        transform.SetParent(transform.root);
        CharacterInventory.characterInventoryInstance.gameObject.GetComponent<CharacterMovement>().canMove = false;
    }
    public void OnDrag(PointerEventData eventData)
    {
        transform.position = Input.mousePosition;
    }
    public void OnEndDrag(PointerEventData eventData)
    {
        if (PetEquipment.petEqipmentinstance == null || !PetEquipment.petEqipmentinstance.gameObject.activeInHierarchy)
        {
            DetectWildPetByray();
            CharacterInventory.characterInventoryInstance.gameObject.GetComponent<CharacterMovement>().canMove = true;
        }
        else
        {
            PetEquipment.petEqipmentinstance.UpdateUI();

        }
        if (GetComponent<ItemButton>().inventoryManager != null)
        {

            GetComponent<ItemButton>().inventoryManager.SortInventoryItem();
        }
        AudioManager.audioSourceInstance.audioSource.PlayOneShot(endDragSound);
        Destroy(eventData.pointerDrag);
    }

    private void DetectWildPetByray()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                if (hit.collider.CompareTag("WildPet"))
                {
                    if (GetComponent<ItemButton>().item.objeto == hit.collider.GetComponent<WildPetController>().petInfo.favoriteFood)
                    {
                        hit.collider.GetComponent<WildPetTamming>().Feed(GetComponent<ItemButton>());

                    }
                }
            }
        }
    }

}
