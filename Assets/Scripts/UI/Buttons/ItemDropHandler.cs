﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ItemDropHandler : MonoBehaviour, IDropHandler
{
    public void OnDrop(PointerEventData eventData)
    {

        if (eventData.pointerDrag.TryGetComponent(out ItemButton buttonDrag))
        {
            if (TryGetComponent(out ItemButton itemButton))
            {
                if (itemButton.item.quickAccess)
                {
                    if (buttonDrag.item.quickAccess)
                    {
                        if (buttonDrag.item.objeto.itemName == itemButton.item.objeto.itemName)
                        {
                            AddAmount(eventData.pointerDrag);
                        }
                        else
                        {
                            ReplaceInQuickAcces(buttonDrag.gameObject);
                        }
                    }
                    else
                    {
                        if (buttonDrag.item.objeto.itemName == itemButton.item.objeto.itemName)
                        {
                            AddAmount(eventData.pointerDrag);
                        }
                        else
                        {
                            itemButton.item.quickAccess = false;
                            buttonDrag.item.quickAccess = true;
                            buttonDrag.item.quickPos = itemButton.item.quickPos;
                        }
                    }
                }
                else
                {
                    if (buttonDrag.item.quickAccess)
                    {
                        if (buttonDrag.item.objeto.itemName == itemButton.item.objeto.itemName)
                        {
                            AddAmount(eventData.pointerDrag);
                        }
                        else
                        {
                            buttonDrag.item.quickAccess = false;
                        }
                    }
                    else
                    {
                        if (buttonDrag.item.objeto.itemName == itemButton.item.objeto.itemName)
                        {
                            AddAmount(eventData.pointerDrag);
                        }
                    }
                }
            }
            else
            {
                if (CompareTag("QuickAccess"))
                {
                    if (!buttonDrag.item.quickAccess)
                    {
                        buttonDrag.item.quickAccess = true;

                    }
                    buttonDrag.item.quickPos = int.Parse(transform.GetComponentInChildren<Text>().text) - 1;
                }
                else
                {
                    if (buttonDrag.item.quickAccess)
                    {
                        buttonDrag.item.quickAccess = false;
                    }
                }
            }
        }

    }

    public void ReplaceInQuickAcces(GameObject dragItem)
    {
        int lastQuickPos = dragItem.GetComponent<ItemButton>().item.quickPos;
        dragItem.GetComponent<ItemButton>().item.quickPos = GetComponent<ItemButton>().item.quickPos;
        GetComponent<ItemButton>().item.quickPos = lastQuickPos;
        QuickAccessButtonManager.quickAccessButtonManagerInstance.OrganizeButtons();
    }
    public void AddAmount(GameObject dragItem)
    {
        if (TryGetComponent(out ItemButton itemButton) && itemButton.item.objeto.stackable)
        {
            itemButton.item.amount += dragItem.GetComponent<ItemButton>().item.amount;
            if (itemButton.item.amount > itemButton.item.objeto.stackAmount)
            {
                int rest = itemButton.item.amount - itemButton.item.objeto.stackAmount;
                itemButton.item.amount = itemButton.item.objeto.stackAmount;
                dragItem.GetComponent<ItemButton>().item.amount = rest;
                if (!itemButton.item.quickAccess)
                {
                    dragItem.GetComponent<ItemButton>().inventory.ItemBack(dragItem.GetComponent<ItemButton>().item);
                }
            }
            else
            {
                dragItem.GetComponent<ItemButton>().inventory.itemsInInventory.Remove(dragItem.GetComponent<ItemButton>().item);
                return;
            }
            QuickAccessButtonManager.quickAccessButtonManagerInstance.OrganizeButtons();
        }

    }
}
