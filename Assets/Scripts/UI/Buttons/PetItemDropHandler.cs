﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class PetItemDropHandler : MonoBehaviour, IDropHandler
{

    public InventoryButtonManager manager;
    public void OnDrop(PointerEventData eventData)
    {
        if (eventData.pointerDrag.TryGetComponent(out ItemButton buttonDrag))
        {
            if (TryGetComponent(out ItemButton itemButton) && (transform.parent.CompareTag("Item") || transform.parent.CompareTag("Armor") || transform.parent.CompareTag("Food")))
            {
                if (buttonDrag.item.objeto.tipo == itemButton.item.objeto.tipo)
                {

                    switch (buttonDrag.item.objeto.tipo)
                    {
                        case ItemInventario.ItemType.Arma:
                            buttonDrag.inventory.itemsInInventory.Add(PetEquipment.petEqipmentinstance.currentPet.item);
                            PetEquipment.petEqipmentinstance.currentPet.item = buttonDrag.item;
                            break;
                        case ItemInventario.ItemType.Consumible:
                            if (buttonDrag.item.objeto == PetEquipment.petEqipmentinstance.currentPet.food.objeto)
                            {
                                int sum = buttonDrag.item.amount + PetEquipment.petEqipmentinstance.currentPet.food.amount;
                                int rest = sum - buttonDrag.item.objeto.stackAmount;
                                if (rest > 0)
                                {
                                    buttonDrag.inventory.AddItem(PetEquipment.petEqipmentinstance.currentPet.food.objeto, rest);
                                    PetEquipment.petEqipmentinstance.currentPet.food.amount = buttonDrag.item.objeto.stackAmount;
                                }
                                else
                                {
                                    PetEquipment.petEqipmentinstance.currentPet.food.amount = sum;
                                }
                            }
                            else
                            {
                                buttonDrag.inventory.AddItem(PetEquipment.petEqipmentinstance.currentPet.food.objeto, PetEquipment.petEqipmentinstance.currentPet.food.amount);
                                PetEquipment.petEqipmentinstance.currentPet.food = buttonDrag.item;
                            }
                            break;
                        case ItemInventario.ItemType.Equipo:
                            buttonDrag.inventory.itemsInInventory.Add(PetEquipment.petEqipmentinstance.currentPet.armor);
                            PetEquipment.petEqipmentinstance.currentPet.armor = buttonDrag.item;
                            break;
                    }
                    if (buttonDrag.inventory != null)
                    {
                        buttonDrag.inventory.itemsInInventory.Remove(buttonDrag.item);
                    }
                }
            }
            else
            {
                if (buttonDrag.TryGetComponent(out ToPetFromCharacterButton toPet))
                {
                    if (CompareTag("Pet"))
                    {
                        toPet.AddToPetBag();
                    }
                }
                else if (buttonDrag.TryGetComponent(out ToCharacterFromPetButton toChar))
                {
                    if (CompareTag("Player"))
                    {
                        toChar.AddToCharacterBag();
                    }
                }
                else if (buttonDrag.TryGetComponent(out EquipItemButton equip))
                {
                    if (CompareTag("Player"))
                    {
                        switch (buttonDrag.item.objeto.tipo)
                        {
                            case ItemInventario.ItemType.Arma:
                                CharacterInventory.characterInventoryInstance.inventory.AddItem(PetEquipment.petEqipmentinstance.currentPet.item.objeto, PetEquipment.petEqipmentinstance.currentPet.item.amount);
                                PetEquipment.petEqipmentinstance.currentPet.item.amount = 0;
                                break;
                            case ItemInventario.ItemType.Consumible:
                                CharacterInventory.characterInventoryInstance.inventory.AddItem(PetEquipment.petEqipmentinstance.currentPet.food.objeto, PetEquipment.petEqipmentinstance.currentPet.food.amount);
                                PetEquipment.petEqipmentinstance.currentPet.food.amount = 0;
                                break;
                            case ItemInventario.ItemType.Equipo:
                                CharacterInventory.characterInventoryInstance.inventory.AddItem(PetEquipment.petEqipmentinstance.currentPet.armor.objeto);
                                PetEquipment.petEqipmentinstance.currentPet.armor.amount = 0;
                                break;
                            default:
                                break;
                        }
                        manager.SortInventoryItem();
                    }
                    else if (CompareTag("Pet"))
                    {
                        switch (buttonDrag.item.objeto.tipo)
                        {
                            case ItemInventario.ItemType.Arma:
                                PetEquipment.petEqipmentinstance.currentPet.inventory.AddItem(PetEquipment.petEqipmentinstance.currentPet.item.objeto, PetEquipment.petEqipmentinstance.currentPet.item.amount);
                                PetEquipment.petEqipmentinstance.currentPet.item.amount = 0;
                                break;
                            case ItemInventario.ItemType.Consumible:
                                PetEquipment.petEqipmentinstance.currentPet.inventory.AddItem(PetEquipment.petEqipmentinstance.currentPet.food.objeto, PetEquipment.petEqipmentinstance.currentPet.food.amount);
                                PetEquipment.petEqipmentinstance.currentPet.food.amount = 0;
                                break;
                            case ItemInventario.ItemType.Equipo:
                                PetEquipment.petEqipmentinstance.currentPet.inventory.AddItem(PetEquipment.petEqipmentinstance.currentPet.armor.objeto);
                                PetEquipment.petEqipmentinstance.currentPet.armor.amount = 0;
                                break;
                            default:
                                break;
                        }
                        manager.SortInventoryItem();
                    }
                    return;
                }
                if (buttonDrag.item.objeto.tipo == ItemInventario.ItemType.Arma && CompareTag("Item"))
                {
                    PetEquipment.petEqipmentinstance.currentPet.item = buttonDrag.item;
                    buttonDrag.inventory.itemsInInventory.Remove(buttonDrag.item);
                }
                else if (buttonDrag.item.objeto.tipo == ItemInventario.ItemType.Consumible && CompareTag("Food"))
                {
                    PetEquipment.petEqipmentinstance.currentPet.food = buttonDrag.item;
                    buttonDrag.inventory.itemsInInventory.Remove(buttonDrag.item);
                }
                else if (buttonDrag.item.objeto.tipo == ItemInventario.ItemType.Equipo && CompareTag("Armor"))
                {
                    PetEquipment.petEqipmentinstance.currentPet.armor = buttonDrag.item;
                    buttonDrag.inventory.itemsInInventory.Remove(buttonDrag.item);
                }
                PetEquipment.petEqipmentinstance.UpdateUI();
            }

        }
    }
}
