﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToCharacterFromPetButton : ItemButton
{
    public InventoryButtonManager characterInventoryManager; 
    //Pasa el item del inventario de la mascota al inventario del Player
    public void AddToCharacterBag()
    {
        CharacterInventory.characterInventoryInstance.inventory.AddItem(item.objeto, item.amount);
        inventory.itemsInInventory.Remove(item);
        inventoryManager.SortInventoryItem();
        characterInventoryManager.SortInventoryItem();
    }
}
