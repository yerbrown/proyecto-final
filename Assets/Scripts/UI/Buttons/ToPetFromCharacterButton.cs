﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToPetFromCharacterButton : ItemButton
{
    public InventoryButtonManager petInventoryManager;
    //Pasa el item de la bolsa del Player a la de la mascota.
    public void AddToPetBag()
    {
        petInventoryManager.inventory.AddItem(item.objeto, item.amount);
        inventory.itemsInInventory.Remove(item);
        inventoryManager.SortInventoryItem();
        petInventoryManager.SortInventoryItem();
    }
}
