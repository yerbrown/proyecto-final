﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UseButton : ItemButton
{
    //Consume el item que contiene el boton y si se queda con 0 borra el boton y actualiza el inventario.
    public void UsarItem()
    {
        item.objeto.Consume();
        item.amount--;
        UpdateButtonUI();
        if (item.amount <= 0)
        {
            inventory.itemsInInventory.Remove(item);

            inventoryManager.OrganizeButtons();
            if (item.quickAccess)
            {
                Destroy(gameObject);
            }
        }
        CharacterAttributeManager.characterAttributeManagerInstance.ActualizarUI();
    }
}
