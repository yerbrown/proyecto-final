﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CraftingManager : MonoBehaviour
{
    [System.Serializable]
    public class CraftingType
    {
        public string typeName;
        public CraftingRecipe.RecipeType type;
        public List<CraftingRecipe> craftingRecipes = new List<CraftingRecipe>();
    }
    public static CraftingManager craftingManagerInstance;
    [Header("Recipes")]
    public List<CraftingType> craftingTypesRecipes = new List<CraftingType>();
    public List<CraftingRecipe> craftingRecipes = new List<CraftingRecipe>();
    public CraftingRecipe currentRecipe;
    [Header("UI")]
    public GameObject recipesContainer;
    public GameObject recipeInfo;
    public Text recipeName;
    public Image recipeImg;
    public Image recipeMaterials;
    public List<GameObject> materials = new List<GameObject>();
    public Button craftButton;
    public GameObject craftButtonPrefab;

    private void Awake()
    {
        craftingManagerInstance = this;
    }

    private void Start()
    {
        UpdateCraftingWindow();
        FilterPerType();
    }
    public void UpdateCraftingWindowPerType(string currentType)
    {
        DeleteButtons();
        for (int i = 0; i < craftingTypesRecipes.Count; i++)
        {
            if (craftingTypesRecipes[i].type.ToString() == currentType)
            {
                for (int j = 0; j < craftingTypesRecipes[i].craftingRecipes.Count; j++)
                {
                    GameObject newCraftButton = Instantiate(craftButtonPrefab, recipesContainer.transform);
                    newCraftButton.GetComponent<RecipeButton>().recipe = craftingTypesRecipes[i].craftingRecipes[j];
                    newCraftButton.transform.GetChild(0).GetComponent<Image>().sprite = craftingTypesRecipes[i].craftingRecipes[j].craftedItem.itemSpr;
                    if (j == 0)
                    {
                        UpdateRecipeWindow(craftingTypesRecipes[i].craftingRecipes[j]);
                    }
                }
                break;
            }
        }


    }
    public void FilterPerType()
    {
        for (int i = 0; i < craftingRecipes.Count; i++)
        {
            for (int j = 0; j < craftingTypesRecipes.Count; j++)
            {
                if (craftingRecipes[i].craftedType == craftingTypesRecipes[j].type)
                {
                    craftingTypesRecipes[j].craftingRecipes.Add(craftingRecipes[i]);
                    break;
                }
            }
        }
    }
    public void UpdateCraftingWindow()
    {
        DeleteButtons();
        for (int i = 0; i < craftingRecipes.Count; i++)
        {
            GameObject newCraftButton = Instantiate(craftButtonPrefab, recipesContainer.transform);
            newCraftButton.GetComponent<RecipeButton>().recipe = craftingRecipes[i];
            newCraftButton.transform.GetChild(0).GetComponent<Image>().sprite = craftingRecipes[i].craftedItem.itemSpr;
            if (i == 0)
            {
                UpdateRecipeWindow(craftingRecipes[i]);
            }
        }
    }
    public void UpdateRecipeWindow(CraftingRecipe recipe)
    {
        currentRecipe = recipe;
        recipeInfo.transform.GetChild(0).GetComponent<LenguageChanger>().keyWord = recipe.craftedItem.itemName;
        recipeInfo.transform.GetChild(0).GetComponent<LenguageChanger>().Traduct();
        recipeInfo.transform.GetChild(1).GetComponent<Text>().text = recipe.craftedItem.itemDescription;
        recipeName.GetComponent<LenguageChanger>().keyWord = recipe.craftedItem.itemName;
        recipeName.GetComponent<LenguageChanger>().Traduct();
        recipeImg.sprite = recipe.craftedItem.itemSpr;
        recipeMaterials.fillAmount = (float)recipe.materialNeeded.Count / 4;
        bool crafteable = true;
        for (int i = 0; i < recipe.materialNeeded.Count; i++)
        {
            materials[i].GetComponent<Image>().sprite = recipe.materialNeeded[i].material.itemSpr;
            int amountInInventory = SearchMaterialsInInventory(recipe.materialNeeded[i].material);
            materials[i].GetComponentInChildren<Text>().text = amountInInventory + "  /  " + recipe.materialNeeded[i].amount;
            if (amountInInventory >= recipe.materialNeeded[i].amount)
            {
                materials[i].GetComponentInChildren<Text>().color = Color.white;
            }
            else
            {
                materials[i].GetComponentInChildren<Text>().color = Color.red;
                crafteable = false;
            }
        }
        if (crafteable)
        {
            craftButton.interactable = true;
        }
        else
        {
            craftButton.interactable = false;
        }

    }
    public int SearchMaterialsInInventory(ItemInventario materialNeeded)
    {
        int amountMaterial = 0;
        for (int i = 0; i < CharacterInventory.characterInventoryInstance.inventory.itemsInInventory.Count; i++)
        {
            if (CharacterInventory.characterInventoryInstance.inventory.itemsInInventory[i].objeto.itemName == materialNeeded.itemName)
            {
                amountMaterial += CharacterInventory.characterInventoryInstance.inventory.itemsInInventory[i].amount;
            }
        }
        return amountMaterial;
    }
    public void DeleteButtons()
    {
        for (int i = recipesContainer.transform.childCount - 1; i >= 0; i--)
        {
            Destroy(recipesContainer.transform.GetChild(i).gameObject);
        }
    }
    public void CraftItem()
    {
        for (int i = 0; i < currentRecipe.materialNeeded.Count; i++)
        {
            int amountNeeded = currentRecipe.materialNeeded[i].amount;
            for (int j = CharacterInventory.characterInventoryInstance.inventory.itemsInInventory.Count - 1; j >= 0; j--)
            {
                if (CharacterInventory.characterInventoryInstance.inventory.itemsInInventory[j].objeto.itemName == currentRecipe.materialNeeded[i].material.itemName)
                {
                    if (CharacterInventory.characterInventoryInstance.inventory.itemsInInventory[j].amount < amountNeeded)
                    {
                        amountNeeded -= CharacterInventory.characterInventoryInstance.inventory.itemsInInventory[j].amount;
                        CharacterInventory.characterInventoryInstance.inventory.itemsInInventory.Remove(CharacterInventory.characterInventoryInstance.inventory.itemsInInventory[j]);
                    }
                    else
                    {
                        CharacterInventory.characterInventoryInstance.inventory.itemsInInventory[j].amount -= amountNeeded;
                        break;
                    }
                }

            }

        }
        CharacterInventory.characterInventoryInstance.inventory.AddItem(currentRecipe.craftedItem);
        UpdateRecipeWindow(currentRecipe);
    }


}
