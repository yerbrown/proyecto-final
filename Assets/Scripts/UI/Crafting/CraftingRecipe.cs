﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Recipe", menuName = "ScriptableObjects/CraftingRecipeScriptableObject", order = 6)]
public class CraftingRecipe : ScriptableObject
{
    [System.Serializable]
    public class MaterialNeeded
    {
        public ItemInventario material;
        public int amount;
    }
    public enum RecipeType
    {
        Armor,
        Tools,
        Food
    }
    [Header("RecipeInfo")]
    public ItemInventario craftedItem;
    public RecipeType craftedType;
    [Header("Materials")]
    public List<MaterialNeeded> materialNeeded = new List<MaterialNeeded>();
}
