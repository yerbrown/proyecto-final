﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecipeButton : MonoBehaviour
{
    public CraftingRecipe recipe;
    public AudioClip clickSound;
    public void SelectRecipe()
    {
        CraftingManager.craftingManagerInstance.UpdateRecipeWindow(recipe);
        AudioManager.audioSourceInstance.audioSource.PlayOneShot(clickSound);
    }
}
