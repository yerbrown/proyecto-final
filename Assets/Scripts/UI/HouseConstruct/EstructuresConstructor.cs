﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EstructuresConstructor : MonoBehaviour
{
    public Text EstructureName;
    public CraftingRecipe currentEstructureCraft;
    public Text material1, material2, material3;
    public Image mat1, mat2, mat3;
    public Button constructButton;

    public GameObject estructureConst;
    public void UpdateUI()
    {
        int currentMaterial;
        bool crafteable = true;
        EstructureName.text = currentEstructureCraft.name;
        currentMaterial = SearchMaterialsInInventory(currentEstructureCraft.materialNeeded[0].material);
        material1.text = currentMaterial + " / " + currentEstructureCraft.materialNeeded[0].amount;
        mat1.sprite = currentEstructureCraft.materialNeeded[0].material.itemSpr;
        if (currentMaterial >= currentEstructureCraft.materialNeeded[0].amount)
        {
            material1.color = Color.white;
        }
        else
        {
            material1.color = Color.red;
            crafteable = false;
        }

        currentMaterial = SearchMaterialsInInventory(currentEstructureCraft.materialNeeded[1].material);
        material2.text = currentMaterial + " / " + currentEstructureCraft.materialNeeded[1].amount;
        mat2.sprite = currentEstructureCraft.materialNeeded[1].material.itemSpr;
        if (currentMaterial >= currentEstructureCraft.materialNeeded[1].amount)
        {
            material2.color = Color.white;
        }
        else
        {
            material2.color = Color.red;
            crafteable = false;
        }

        currentMaterial = SearchMaterialsInInventory(currentEstructureCraft.materialNeeded[2].material);
        material3.text = currentMaterial + " / " + currentEstructureCraft.materialNeeded[2].amount;
        mat3.sprite = currentEstructureCraft.materialNeeded[2].material.itemSpr;
        if (currentMaterial >= currentEstructureCraft.materialNeeded[2].amount)
        {
            material3.color = Color.white;
        }
        else
        {
            material3.color = Color.red;
            crafteable = false;
        }

        if (crafteable)
        {
            constructButton.interactable = true;
        }
        else
        {
            constructButton.interactable = false;

        }
    }

    public int SearchMaterialsInInventory(ItemInventario materialNeeded)
    {
        int amountMaterial = 0;
        for (int i = 0; i < CharacterInventory.characterInventoryInstance.inventory.itemsInInventory.Count; i++)
        {
            if (CharacterInventory.characterInventoryInstance.inventory.itemsInInventory[i].objeto.itemName == materialNeeded.itemName)
            {
                amountMaterial += CharacterInventory.characterInventoryInstance.inventory.itemsInInventory[i].amount;
            }
        }
        return amountMaterial;
    }

    public void RestAmount()
    {

    }
}
