﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{

    public void CargarPartida()
    {
        loadScenes.loadScenesInstance.CargarPartida(1);
    }

    public void Cerrar()
    {
        Application.Quit();
    }


}
