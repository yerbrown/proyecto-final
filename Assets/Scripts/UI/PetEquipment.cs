﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetEquipment : MonoBehaviour
{
    public static PetEquipment petEqipmentinstance;
    public AnimalsController currentPet;
    public GameObject armor, item, food;
    public GameObject prefabButton;

    private void Awake()
    {
        petEqipmentinstance = this;
    }
    private void OnEnable()
    {
        currentPet = PetManager.petManagerInstance.selectedPet;
    }
    public void UpdateUI()
    {
        if (armor.transform.childCount > 1)
        {
            Destroy(armor.transform.GetChild(1).gameObject);
        }
        if (item.transform.childCount > 1)
        {
            Destroy(item.transform.GetChild(1).gameObject);
        }
        if (food.transform.childCount > 1)
        {
            Destroy(food.transform.GetChild(1).gameObject);
        }

        if (currentPet.armor.amount > 0)
        {
            GameObject newButoon = Instantiate(prefabButton, armor.transform);
            newButoon.GetComponent<EquipItemButton>().item = currentPet.armor;
            newButoon.GetComponent<EquipItemButton>().UpdateButtonUI();
        }

        if (currentPet.item.amount > 0)
        {
            GameObject newButoon = Instantiate(prefabButton, item.transform);
            newButoon.GetComponent<EquipItemButton>().item = currentPet.item;
            newButoon.GetComponent<EquipItemButton>().UpdateButtonUI();
        }
        if (currentPet.food.amount > 0)
        {
            GameObject newButoon = Instantiate(prefabButton, food.transform);
            newButoon.GetComponent<EquipItemButton>().item = currentPet.food;
            newButoon.GetComponent<EquipItemButton>().UpdateButtonUI();
        }
    }
}
