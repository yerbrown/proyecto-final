﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LenguageChanger : MonoBehaviour
{
    public string keyWord;
    public int amount;
    private void Start()
    {
        if (keyWord == "")
        {
            keyWord = GetComponent<Text>().text;
        }        
        Traduct();
    }
    public void Traduct()
    {
        if (name == "Label")
        {
            keyWord = TextReader.textReaderInstance.lenguage.ToString();
        }
        GetComponent<Text>().text = TextReader.textReaderInstance.SearchWord(keyWord);
        if (amount > 1)
        {
            GetComponent<Text>().text += " x" + amount;
        }
        amount = 0;
    }
    private void OnEnable()
    {
        Traduct();
        TextReader.textReaderInstance.onLenguageChange.AddListener(Traduct);
    }
}
