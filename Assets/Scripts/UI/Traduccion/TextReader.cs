﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.TextCore;

public class TextReader : MonoBehaviour
{
    public static TextReader textReaderInstance;
    [HideInInspector]
    public UnityEvent onLenguageChange;
    [System.Serializable]
    public class DictionaryWord
    {
        public string keyWord;
        public string[] words;
        public Dictionary<string, string> wordPerLenguage = new Dictionary<string, string>();
        public DictionaryWord(string fila)
        {
            words = fila.Split(";"[0]);
            keyWord = words[0];
        }
    }
    public DictionaryWord[] wordFila;
    public TextAsset textFile;
    public string[] fila;
    public string[,] dictionaryDoble;
    public DictionaryLenguages lenguage;
    public enum DictionaryLenguages
    {
        Key,
        English,
        Spanish,
        French,
        German,
        Portuguese
    }

    private Dictionary<string, string> columas = new Dictionary<string, string>();
    private void Awake()
    {
        textReaderInstance = this;
        ReadFile();
    }

    public void ReadFile()
    {
        fila = textFile.text.Split("\n"[0]);
        wordFila = new DictionaryWord[fila.Length];
        for (int i = 0; i < fila.Length; i++)
        {
            wordFila[i] = new DictionaryWord(fila[i]);
            for (int j = 0; j < wordFila[0].words.Length; j++)
            {
                string lenguage = wordFila[0].words[j];
                if (lenguage.Contains("\r"))
                {
                    lenguage = lenguage.Replace("\r", "");
                }
                wordFila[i].wordPerLenguage.Add(lenguage, wordFila[i].words[j]);
            }
        }
    }

    public string SearchWord(string wordKey)
    {
        for (int i = 0; i < wordFila.Length; i++)
        {
            if (wordFila[i].keyWord == wordKey)
            {
                return wordFila[i].wordPerLenguage[lenguage.ToString()];
            }
        }
        //Debug.Log("No se encontro esa palabra");
        return null;
    }
    public void ChangeLenguage(int value)
    {
        lenguage = (DictionaryLenguages)value + 1;
        onLenguageChange.Invoke();
    }
}
