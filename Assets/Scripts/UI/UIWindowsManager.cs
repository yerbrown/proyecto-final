﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIWindowsManager : MonoBehaviour
{
    public static UIWindowsManager windowsManagerInstance;
    public GameObject currentWindow;
    public bool currentTimeScale = false;
    public GameObject pauseMenu;
    public KeyCode keyToPause;
    private void Awake()
    {
        windowsManagerInstance = this;
    }
    private void Update()
    {
        if (Input.GetKeyDown(keyToPause))
        {
            PauseOptions();
        }


    }
    public void ChangeWindow(GameObject nextWindow)
    {
        if (currentWindow != null)
        {
            currentWindow.SetActive(false);
        }
        nextWindow.SetActive(true);
        currentWindow = nextWindow;
        
    }
    public void ChangeTimeScale(bool pause)
    {
        if (pause)
        {
            Time.timeScale = 0;
            currentTimeScale = true;
        }
        else
        {
            Time.timeScale = 1;
            currentTimeScale = false;
        }
    }
    public void PauseOptions()
    {
        pauseMenu.SetActive(!pauseMenu.activeSelf);
        if (pauseMenu.activeSelf)
        {
            Time.timeScale = 0;
        }
        else
        {
            if (currentTimeScale)
            {
                Time.timeScale = 0;
            }
            else
            {
                Time.timeScale = 1;
            }
        }
    }
}
