﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Events;
public class loadScenes : MonoBehaviour
{
    public static loadScenes loadScenesInstance;
    public Slider loadSlider;
    public UnityEvent saveData;

    private void Awake()
    {
        loadScenesInstance = this;
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            saveData.Invoke();
        }
    }

    public void CargarPartida(int sceneNum)
    {
        StartCoroutine(LoadAsynchronously(sceneNum));
        saveData.Invoke();
    }

    IEnumerator LoadAsynchronously(int sceneNum)
    {
        AsyncOperation loading = SceneManager.LoadSceneAsync(sceneNum, LoadSceneMode.Single);
        while (!loading.isDone)
        {
            float progress = Mathf.Clamp01(loading.progress / 0.9f);
            loadSlider.value = progress;
            yield return null;
        }
    }
}
